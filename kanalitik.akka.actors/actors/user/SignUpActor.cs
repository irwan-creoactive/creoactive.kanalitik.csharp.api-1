﻿using Akka;
using Akka.Actor;
using kanalitik.data.contract;
using kanalitik.data.contract.apiRequest;
using kanalitik.interfaces;
using kanalitik.repositories;
using kanalitik.request.user;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kanalitik.akka.actors.user
{
    public class SignUpActor : UntypedActor
    {
        IUserService userSvc;
        public SignUpActor(IUserService _userSvc)
        {
            userSvc = _userSvc;
        }

        protected override void OnReceive(object request)
        {
            var apiReq = (RegisterRequestApi)request;

            var actorReq = new ActorRequest<RegisterRequest>();
            // Link from Api Request to Actor Request
            actorReq.data = new RegisterRequest { 
                CultureName = apiReq.CultureName,
                name = apiReq.name,
                email = apiReq.email,
                business_type = apiReq.business_type,
                business_type_name = apiReq.business_type_name,
                UserId=apiReq.UserId ,
                contact_number = apiReq.contact_number
            };

            var res = userSvc.register(actorReq);
            Sender.Forward(res);
        }
    }

}
