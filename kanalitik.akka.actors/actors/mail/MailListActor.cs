﻿using Akka;
using Akka.Actor;
using kanalitik.data.contract;
using kanalitik.data.contract.apiRequest;
using kanalitik.interfaces;
using kanalitik.repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kanalitik.akka.actors.mail
{
    public class MailListActor : UntypedActor
    {
        IMailService svc;
        public MailListActor(IMailService _svc)
        {
            svc = _svc;
        }

        protected override void OnReceive(object request)
        {
            var req = (ActorRequest<MailRequest>)request;
            var res = svc.mailList(req);
            Sender.Forward(res);
        }
    }

}
