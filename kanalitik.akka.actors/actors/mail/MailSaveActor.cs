﻿using Akka;
using Akka.Actor;
using kanalitik.data.contract;
using kanalitik.data.contract.apiRequest;
using kanalitik.interfaces;
using kanalitik.repositories;
using kanalitik.request.mail;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kanalitik.akka.actors.mail
{
    public class MailSaveActor : UntypedActor
    {
        IMailService svc;
        public MailSaveActor(IMailService _svc)
        {
            svc = _svc;
        }

        protected override void OnReceive(object request)
        {
            var apiReq = (MailSaveRequestApi)request;

            var actorReq = new ActorRequest<MailSaveRequest>();
            // Link from Api Request to Actor Request
            actorReq.data = new MailSaveRequest
            {
                CultureName = apiReq.CultureName,
                UserId = apiReq.UserId,
                email = apiReq.email,
                TokenUser = "",

                configCode = apiReq.configCode,
            };

            var res = svc.mailSave(actorReq);
            Sender.Forward(res);
        }
    }

}
