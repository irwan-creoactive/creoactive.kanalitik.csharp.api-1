﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using kanalitik.api.Controllers;
using kanalitik.akka.actors.core;
namespace kanalitik.api.Tests.Controllers
{
    [TestClass]
    public class UserControllerTest
    {
        public UserControllerTest()
        {
            BootstrapActor.Initialize();
        }
        [TestMethod]
        public void registerCompanyTest()
        {
            

            UserController userCtl = new UserController();
            var actual = userCtl.register(new kanalitik.data.contract.apiRequest.RegisterRequestApi 
            {
                CultureName = "en-US",
                email="company@mail.com",
                business_type="Company",
                business_type_name="PT Maju mundur",
                contact_number="xxx123",
                UserId = "company@mail.com",
                name="PT Maju Mundur"
            }).GetAwaiter().GetResult();

            Assert.AreEqual("OK", actual.Result.message);
            Assert.AreEqual(true, actual.Result.success);
            Assert.AreEqual(true, actual.Result.status);

        }

         [TestMethod]
        public void registerOrganizationTest()
        {
            

            UserController userCtl = new UserController();
            var actual = userCtl.register(new kanalitik.data.contract.apiRequest.RegisterRequestApi
            {
                CultureName = "en-US",
                email = "organization@mail.com",
                business_type = "Organization",
                contact_number = "xxx123",
                UserId = "company@mail.com",
                name = "PT Maju Mundur"
            }).GetAwaiter().GetResult();

            Assert.AreEqual("OK", actual.Result.message);
            Assert.AreEqual(true, actual.Result.success);
            Assert.AreEqual(true, actual.Result.status);

        }

        [TestMethod]
        public void registerPersonalTest()
        {
            

            UserController userCtl = new UserController();
            var actual = userCtl.register(new kanalitik.data.contract.apiRequest.RegisterRequestApi
            {
                CultureName = "en-US",
                email = "personal@mail.com",
                business_type = "Personal",
                contact_number = "xxx123",
                UserId = "company@mail.com",
                name = "PT Maju Mundur"
            }).GetAwaiter().GetResult();

            Assert.AreEqual("OK", actual.Result.message);
            Assert.AreEqual(true, actual.Result.success);
            Assert.AreEqual(true, actual.Result.status);

        }
    }
}
