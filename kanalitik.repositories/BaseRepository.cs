﻿using kanalitik.data.access;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace kanalitik.repositories
{
    public class BaseRepository
    {
        public DBKanalitikContext db{set;get;}
        
        public BaseRepository()
        {
            db = new DBKanalitikContext();            
        }
    }
}
