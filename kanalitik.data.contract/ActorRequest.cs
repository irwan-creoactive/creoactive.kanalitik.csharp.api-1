﻿using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace kanalitik.data.contract
{
    public class ActorRequest<T> 
   {
       ConfigurationValidatorFactory factory;
       public string FileValidationBlock { set; get; }
       public ActorRequest()
       {
           string cultureName = Thread.CurrentThread.CurrentCulture.Name;
           this.FileValidationBlock = typeof(T).Name;
           this.setConfigValidationBlock(cultureName, this.FileValidationBlock);
       }
       protected void setConfigValidationBlock(string cultureName, string configfileValidationBlock)
       {
           if (cultureName == null)
           {
               cultureName = Thread.CurrentThread.CurrentCulture.Name;
           }
           string fileConfig = kanalitik.constanta.Const.PathBaseDir +
                "Resources\\" + cultureName + @"\" + configfileValidationBlock + ".config";
           if (!System.IO.File.Exists(fileConfig))
           {
               throw new Exception("File Config Model Validation Block not exist");
           }
           var cfgSrc = new FileConfigurationSource(
               fileConfig
               );
           factory = ConfigurationValidatorFactory.FromConfigurationSource(cfgSrc);
       }
       public ValidationResults validateModel(T modelToValidate, string cultureName, string ruleSet)
       {
          

           this.setConfigValidationBlock(cultureName, this.FileValidationBlock);

           Validator<T> valFactory = factory.CreateValidator<T>(ruleSet);
           return valFactory.Validate(modelToValidate);
       }
       public ValidationResults validateModel(T modelToValidate, string ruleSet)
       {
           Validator<T> valFactory = factory.CreateValidator<T>(ruleSet);
           return valFactory.Validate(modelToValidate);
       }

       public void throwErrorSystem(ValidationResults validations)
       {
           throw new Exception("{\"validator\":[ \"" +
                   String.Join("\",\"", validations.Select(a => a.Message).ToArray()) + "\"]}"
                   );
       }
       public T data { set; get; }
   }
}
