﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kanalitik.data.contract.apiRequest
{
    public class BlogCommentRequestApi : BaseRequest
    {
        public string BlogId { set; get; }
        public string Seq { set; get; }
        public string Email { set; get; }
        public string Name { set; get; }
        public string Comment { set; get; }
        public string IsApprove { set; get; }
    }
}
