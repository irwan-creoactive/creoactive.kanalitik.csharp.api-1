﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kanalitik.data.contract.apiRequest
{
    public class AccountSettingRequest : BaseRequest
    {
        public string Name { set; get; }
        public string Email { set; get; }
        public string Password { set; get; }
        public string ContactNumber { set; get; }
    }
}
