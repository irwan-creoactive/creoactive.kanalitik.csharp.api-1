﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kanalitik.data.contract.apiRequest
{
    public class PackageRequest : BaseRequest
    {
        public string package { set; get; }
    }
}
