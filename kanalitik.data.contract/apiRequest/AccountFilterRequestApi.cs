﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kanalitik.data.contract.apiRequest
{
    public class AccountFilterRequestApi : BaseRequest
    {
        public string email { set; get; }
        public string Media { set; get; }
        public string IsNonAuthorize { set; get; }
    }
}
