﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kanalitik.data.contract.apiRequest
{
    public class MasterDataPackageRequest : BaseRequest
    {
        public double price { get; set; }
        public string[] social_media { get; set; }
        public string[] blog { set; get; }
        public string[] news_portal { set; get; }
        public bool is_show { set; get; }
        //public List<ModulePackageRequest> module { set; get; }
    }

    //public class ModulePackageRequest
    //{
    //    string module_id { set; get; }
    //    bool can_change { set; get; }
    //    bool can_view { set; get; }
    //    bool can_remove { set; get; }
    //}
}
