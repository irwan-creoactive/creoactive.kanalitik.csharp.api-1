﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kanalitik.data.contract.apiResponse
{
    public class BlogResponse : BaseResponse
    {
        public string _id { set; get; }
        public string Title { set; get; }
        public string Content { set; get; }
        public string Image { set; get; }
        public bool IsPublish { set; get; }
    }

    public class BlogCommentResponse : BaseResponse
    {
        public string BlogId { set; get; }
        public string Seq { set; get; }
        public string Email { set; get; }
        public string Name { set; get; }
        public string Comment { set; get; }
        public string IsApprove { set; get; }
    }
}
