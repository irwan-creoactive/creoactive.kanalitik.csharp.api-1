﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kanalitik.data.contract.apiResponse
{
    public class OrderPackageResponse : BaseResponse
    {
        public string Id { set; get; }
    }
}
