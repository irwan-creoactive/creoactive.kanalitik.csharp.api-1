﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kanalitik.data.contract.apiResponse
{
    public class ClientSettingResponse : BaseResponse
    {
        public string ClientID { set; get; }
        public string UserId { set; get; }
    }
}
