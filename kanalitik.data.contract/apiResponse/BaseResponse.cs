﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kanalitik.data.contract.apiResponse
{
    public class BaseResponse
    {
        public string message { set; get; }
        public string messageCode { set; get; }
        public bool status { set; get; }
        public bool success { set; get; }
    }
}
