﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kanalitik.data.contract.apiResponse
{
    public class StreamResponse : BaseResponse
    {
        public string StreamId { set; get; }
        public string Media { set; get; }
        public string Group { set; get; }
        public string StreamName { set; get; }
        public string UrlWeb { set; get; }
        public string UrlRSS { set; get; }
        public string Categories { set; get; }
        public string Contents { set; get; }
    }
}
