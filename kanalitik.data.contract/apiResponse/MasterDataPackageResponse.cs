﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kanalitik.data.contract.apiResponse
{
    public class MasterDataPackageResponse : BaseResponse
    {
        public string Id { set; get; }
    }
}
