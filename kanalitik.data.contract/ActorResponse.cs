﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kanalitik.data.contract
{
    public class ActorResponse<T>
    {
        public ActorResponse()
        {
         
        }
        public T Result { set; get; }
    }
}
