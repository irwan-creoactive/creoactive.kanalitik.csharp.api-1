﻿using kanalitik.data.access.documents;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace kanalitik.models
{


    public class ConfigMessageContentModel : BaseModel<ConfigMessageContent>, IBaseModel<ConfigMessageContent>
    {

        public ConfigMessageContentModel() : base("ConfigMessageContentModel") { }

        public async Task<ConfigMessageContent> createOne(ConfigMessageContent data)
        {
            // Jangan Lupa pastikan RuleSet ada!!
            var validation = this.validateModel(data, "ConfigMessageContentInsertRuleSet");
            Task<ConfigMessageContent> result = null;
            if (validation.IsValid)
            {
                await this.db.ConfigMessageContent.InsertOneAsync(data);
                result = Task<ConfigMessageContent>.Run(() =>
                {
                    return data;
                });
            }
            else
            {
                this.throwErrorSystem(validation);
            }
            return await result;
        }

        public async Task<ConfigMessageContent> updateOne(ConfigMessageContent data)
        {
            // Jangan Lupa pastikan RuleSet ada!!
            var validation = this.validateModel(data, "ConfigMessageContentUpdateRuleSet");

            Task<ConfigMessageContent> result = null;
            if (validation.IsValid)
            {
                var updateBuilder = new UpdateBuilderModel<ConfigMessageContent>().fieldSet(data);
                var whereclause = new BsonDocument(new BsonElement("_id", data.id)).ToJson();
                await this.db.ConfigMessageContent.FindOneAndUpdateAsync(whereclause, updateBuilder);
                result = Task<ConfigMessageContent>.Run(() =>
                {
                    return data;
                });
            }
            else
            {
                this.throwErrorSystem(validation);
            }
            return await result;
        }

        public async Task createMany(List<ConfigMessageContent> data)
        {
            // Jangan Lupa pastikan RuleSet ada!!
            var success = 0;
            foreach (var item in data)
            {
                var validation = this.validateModel(item, "ConfigMessageContentInsertRuleSet");
                if (!validation.IsValid)
                {
                    this.throwErrorSystem(validation);
                    break;
                }
                success = 1;
            }
            if (success == 1)
            {
                await this.db.ConfigMessageContent.InsertManyAsync(data);
            }

        }

        //TODO
        public Task updateMany(List<ConfigMessageContent> data)
        {
            throw new NotImplementedException();
        }

        public async Task<ConfigMessageContent> getById(string id)
        {
            var builderFilter = Builders<ConfigMessageContent>.Filter;
            var filter = builderFilter.Eq(a => a.id, new ObjectId(id));

            Task<ConfigMessageContent> result = null;
            using (var cursor = await this.db.ConfigMessageContent.Find(filter).ToCursorAsync())
            {
                while (await cursor.MoveNextAsync())
                {
                    foreach (var doc in cursor.Current)
                    {
                        if (doc != null)
                        {
                            result = Task<ConfigMessageContent>.Run(() =>
                            {
                                return doc;
                            });
                        }
                    }
                }
            }
            return await result;
        }

        public async Task<string> deleteById(string id)
        {
            var builderFilter = Builders<ConfigMessageContent>.Filter.Eq(a => a.id, new ObjectId(id));
            await this.db.ConfigMessageContent.FindOneAndDeleteAsync(builderFilter);
            Task<string> result = Task<string>.Run(() =>
            {
                return id;
            });
            return await result;
        }

        public async Task<ConfigMessageContent[]> list(BsonDocument filter, BsonDocument sort, int skip, int limit)
        {

            Task<ConfigMessageContent[]> result = null;
            List<ConfigMessageContent> res = new List<ConfigMessageContent>();
            using (var cursor = await this.db.ConfigMessageContent.Find(filter).Sort(sort).Limit(limit).Skip(skip).ToCursorAsync())
            {
                while (await cursor.MoveNextAsync())
                {
                    foreach (var doc in cursor.Current)
                    {
                        res.Add(doc);
                    }
                }
            }

            result = Task<ConfigMessageContent[]>.Run(() =>
            {
                return res.ToArray();
            });


            return await result;
        }

    }
}
