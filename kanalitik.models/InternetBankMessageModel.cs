﻿using kanalitik.data.access.documents;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace kanalitik.models
{


    public class InternetBankMessageModel : BaseModel<InternetBankMessage>, IBaseModel<InternetBankMessage>
    {

        public InternetBankMessageModel() : base("InternetBankMessageModel") { }

        public async Task<InternetBankMessage> createOne(InternetBankMessage data)
        {
            // Jangan Lupa pastikan RuleSet ada!!
            var validation = this.validateModel(data, "CreateRuleSet");
            Task<InternetBankMessage> result = null;
            if (validation.IsValid)
            {
                await this.db.InternetBankMessage.InsertOneAsync(data);
                result = Task<InternetBankMessage>.Run(() =>
                {
                    return data;
                });
            }
            else
            {
                this.throwErrorSystem(validation);
            }
            return await result;
        }

        public async Task<InternetBankMessage> updateOne(InternetBankMessage data)
        {
            // Jangan Lupa pastikan RuleSet ada!!
            var validation = this.validateModel(data, "UpdateRuleSet");

            Task<InternetBankMessage> result = null;
            if (validation.IsValid)
            {
                var updateBuilder = new UpdateBuilderModel<InternetBankMessage>().fieldSet(data);
                var whereclause = new BsonDocument(new BsonElement("_id", data.id)).ToJson();
                await this.db.InternetBankMessage.FindOneAndUpdateAsync(whereclause, updateBuilder);
                result = Task<InternetBankMessage>.Run(() =>
                {
                    return data;
                });
            }
            else
            {
                this.throwErrorSystem(validation);
            }
            return await result;
        }

        public async Task createMany(List<InternetBankMessage> data)
        {
            // Jangan Lupa pastikan RuleSet ada!!
            var success = 0;
            foreach (var item in data)
            {
                var validation = this.validateModel(item, "CreateRuleSet");
                if (!validation.IsValid)
                {
                    this.throwErrorSystem(validation);
                    break;
                }
                success = 1;
            }
            if (success == 1)
            {
                await this.db.InternetBankMessage.InsertManyAsync(data);
            }

        }

        //TODO
        public Task updateMany(List<InternetBankMessage> data)
        {
            throw new NotImplementedException();
        }

        public async Task<InternetBankMessage> getById(string id)
        {
            var builderFilter = Builders<InternetBankMessage>.Filter;
            var filter = builderFilter.Eq(a => a.id, new ObjectId(id));

            Task<InternetBankMessage> result = null;
            using (var cursor = await this.db.InternetBankMessage.Find(filter).ToCursorAsync())
            {
                while (await cursor.MoveNextAsync())
                {
                    foreach (var doc in cursor.Current)
                    {
                        if (doc != null)
                        {
                            result = Task<InternetBankMessage>.Run(() =>
                            {
                                return doc;
                            });
                        }
                    }
                }
            }
            return await result;
        }

        public async Task<string> deleteById(string id)
        {
            var builderFilter = Builders<InternetBankMessage>.Filter.Eq(a => a.id, new ObjectId(id));
            await this.db.InternetBankMessage.FindOneAndDeleteAsync(builderFilter);
            Task<string> result = Task<string>.Run(() =>
            {
                return id;
            });
            return await result;
        }

        public async Task<InternetBankMessage[]> list(BsonDocument filter, BsonDocument sort, int skip, int limit)
        {

            Task<InternetBankMessage[]> result = null;
            List<InternetBankMessage> res = new List<InternetBankMessage>();
            using (var cursor = await this.db.InternetBankMessage.Find(filter).Sort(sort).Limit(limit).Skip(skip).ToCursorAsync())
            {
                while (await cursor.MoveNextAsync())
                {
                    foreach (var doc in cursor.Current)
                    {
                        res.Add(doc);
                    }
                }
            }

            result = Task<InternetBankMessage[]>.Run(() =>
            {
                return res.ToArray();
            });


            return await result;
        }

    }
}
