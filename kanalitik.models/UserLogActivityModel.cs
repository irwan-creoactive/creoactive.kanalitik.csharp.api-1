﻿using kanalitik.data.access.documents;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace kanalitik.models
{


    public class UserLogActivityModel : BaseModel<UserLogActivity>, IBaseModel<UserLogActivity>
    {

        public UserLogActivityModel() : base("UserLogActivityModel") { }

        public async Task<UserLogActivity> createOne(UserLogActivity data)
        {
            // Jangan Lupa pastikan RuleSet ada!!
            var validation = this.validateModel(data, "UserLogActivityInsertRuleSet");
            Task<UserLogActivity> result = null;
            if (validation.IsValid)
            {
                await this.db.UserLogActivity.InsertOneAsync(data);
                result = Task<UserLogActivity>.Run(() =>
                {
                    return data;
                });
            }
            else
            {
                this.throwErrorSystem(validation);
            }
            return await result;
        }

        public async Task<UserLogActivity> updateOne(UserLogActivity data)
        {
            // Jangan Lupa pastikan RuleSet ada!!
            var validation = this.validateModel(data, "UserLogActivityUpdateRuleSet");

            Task<UserLogActivity> result = null;
            if (validation.IsValid)
            {
                var updateBuilder = new UpdateBuilderModel<UserLogActivity>().fieldSet(data);
                var whereclause = new BsonDocument(new BsonElement("_id", data.id)).ToJson();
                await this.db.UserLogActivity.FindOneAndUpdateAsync(whereclause, updateBuilder);
                result = Task<UserLogActivity>.Run(() =>
                {
                    return data;
                });
            }
            else
            {
                this.throwErrorSystem(validation);
            }
            return await result;
        }

        public async Task createMany(List<UserLogActivity> data)
        {
            // Jangan Lupa pastikan RuleSet ada!!
            var success = 0;
            foreach (var item in data)
            {
                var validation = this.validateModel(item, "UserLogActivityCreateRuleSet");
                if (!validation.IsValid)
                {
                    this.throwErrorSystem(validation);
                    break;
                }
                success = 1;
            }
            if (success == 1)
            {
                await this.db.UserLogActivity.InsertManyAsync(data);
            }

        }

        //TODO
        public Task updateMany(List<UserLogActivity> data)
        {
            throw new NotImplementedException();
        }

        public async Task<UserLogActivity> getById(string id)
        {
            var builderFilter = Builders<UserLogActivity>.Filter;
            var filter = builderFilter.Eq(a => a.id, new ObjectId(id));

            Task<UserLogActivity> result = null;
            using (var cursor = await this.db.UserLogActivity.Find(filter).ToCursorAsync())
            {
                while (await cursor.MoveNextAsync())
                {
                    foreach (var doc in cursor.Current)
                    {
                        if (doc != null)
                        {
                            result = Task<UserLogActivity>.Run(() =>
                            {
                                return doc;
                            });
                        }
                    }
                }
            }
            return await result;
        }

        public async Task<string> deleteById(string id)
        {
            var builderFilter = Builders<UserLogActivity>.Filter.Eq(a => a.id, new ObjectId(id));
            await this.db.UserLogActivity.FindOneAndDeleteAsync(builderFilter);
            Task<string> result = Task<string>.Run(() =>
            {
                return id;
            });
            return await result;
        }

        public async Task<UserLogActivity[]> list(BsonDocument filter, BsonDocument sort, int skip, int limit)
        {

            Task<UserLogActivity[]> result = null;
            List<UserLogActivity> res = new List<UserLogActivity>();
            using (var cursor = await this.db.UserLogActivity.Find(filter).Sort(sort).Limit(limit).Skip(skip).ToCursorAsync())
            {
                while (await cursor.MoveNextAsync())
                {
                    foreach (var doc in cursor.Current)
                    {
                        res.Add(doc);
                    }
                }
            }

            result = Task<UserLogActivity[]>.Run(() =>
            {
                return res.ToArray();
            });


            return await result;
        }

    }
}
