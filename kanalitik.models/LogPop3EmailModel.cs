﻿using kanalitik.data.access.documents;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace kanalitik.models
{


    public class LogPop3EmailModel : BaseModel<LogPop3Email>, IBaseModel<LogPop3Email>
    {

        public LogPop3EmailModel() : base("LogPop3EmailModel") { }

        public async Task<LogPop3Email> createOne(LogPop3Email data)
        {
            // Jangan Lupa pastikan RuleSet ada!!
            var validation = this.validateModel(data, "LogPop3EmailInsertRuleSet");
            Task<LogPop3Email> result = null;
            if (validation.IsValid)
            {
                await this.db.LogPop3Email.InsertOneAsync(data);
                result = Task<LogPop3Email>.Run(() =>
                {
                    return data;
                });
            }
            else
            {
                this.throwErrorSystem(validation);
            }
            return await result;
        }

        public async Task<LogPop3Email> updateOne(LogPop3Email data)
        {
            // Jangan Lupa pastikan RuleSet ada!!
            var validation = this.validateModel(data, "LogPop3EmailUpdateRuleSet");

            Task<LogPop3Email> result = null;
            if (validation.IsValid)
            {
                var updateBuilder = new UpdateBuilderModel<LogPop3Email>().fieldSet(data);
                var whereclause = new BsonDocument(new BsonElement("_id", data.id)).ToJson();
                await this.db.LogPop3Email.FindOneAndUpdateAsync(whereclause, updateBuilder);
                result = Task<LogPop3Email>.Run(() =>
                {
                    return data;
                });
            }
            else
            {
                this.throwErrorSystem(validation);
            }
            return await result;
        }

        public async Task createMany(List<LogPop3Email> data)
        {
            // Jangan Lupa pastikan RuleSet ada!!
            var success = 0;
            foreach (var item in data)
            {
                var validation = this.validateModel(item, "LogPop3EmailInsertRuleSet");
                if (!validation.IsValid)
                {
                    this.throwErrorSystem(validation);
                    break;
                }
                success = 1;
            }
            if (success == 1)
            {
                await this.db.LogPop3Email.InsertManyAsync(data);
            }

        }

        //TODO
        public Task updateMany(List<LogPop3Email> data)
        {
            throw new NotImplementedException();
        }

        public async Task<LogPop3Email> getById(string id)
        {
            var builderFilter = Builders<LogPop3Email>.Filter;
            var filter = builderFilter.Eq(a => a.id, new ObjectId(id));

            Task<LogPop3Email> result = null;
            using (var cursor = await this.db.LogPop3Email.Find(filter).ToCursorAsync())
            {
                while (await cursor.MoveNextAsync())
                {
                    foreach (var doc in cursor.Current)
                    {
                        if (doc != null)
                        {
                            result = Task<LogPop3Email>.Run(() =>
                            {
                                return doc;
                            });
                        }
                    }
                }
            }
            return await result;
        }

        public async Task<string> deleteById(string id)
        {
            var builderFilter = Builders<LogPop3Email>.Filter.Eq(a => a.id, new ObjectId(id));
            await this.db.LogPop3Email.FindOneAndDeleteAsync(builderFilter);
            Task<string> result = Task<string>.Run(() =>
            {
                return id;
            });
            return await result;
        }

        public async Task<LogPop3Email[]> list(BsonDocument filter, BsonDocument sort, int skip, int limit)
        {

            Task<LogPop3Email[]> result = null;
            List<LogPop3Email> res = new List<LogPop3Email>();
            using (var cursor = await this.db.LogPop3Email.Find(filter).Sort(sort).Limit(limit).Skip(skip).ToCursorAsync())
            {
                while (await cursor.MoveNextAsync())
                {
                    foreach (var doc in cursor.Current)
                    {
                        res.Add(doc);
                    }
                }
            }

            result = Task<LogPop3Email[]>.Run(() =>
            {
                return res.ToArray();
            });


            return await result;
        }

    }
}
