﻿using kanalitik.data.access.documents;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace kanalitik.models
{


    public class StreamUnwantedModel : BaseModel<StreamUnwanted>, IBaseModel<StreamUnwanted>
    {

        public StreamUnwantedModel() : base("StreamUnwantedModel") { }

        public async Task<StreamUnwanted> createOne(StreamUnwanted data)
        {
            // Jangan Lupa pastikan RuleSet ada!!
            var validation = this.validateModel(data, "StreamUnwantedInsertRuleSet");
            Task<StreamUnwanted> result = null;
            if (validation.IsValid)
            {
                await this.db.StreamUnwanted.InsertOneAsync(data);
                result = Task<StreamUnwanted>.Run(() =>
                {
                    return data;
                });
            }
            else
            {
                this.throwErrorSystem(validation);
            }
            return await result;
        }

        public async Task<StreamUnwanted> updateOne(StreamUnwanted data)
        {
            // Jangan Lupa pastikan RuleSet ada!!
            var validation = this.validateModel(data, "StreamUnwantedUpdateRuleSet");

            Task<StreamUnwanted> result = null;
            if (validation.IsValid)
            {
                var updateBuilder = new UpdateBuilderModel<StreamUnwanted>().fieldSet(data);
                var whereclause = new BsonDocument(new BsonElement("_id", data.id)).ToJson();
                await this.db.StreamUnwanted.FindOneAndUpdateAsync(whereclause, updateBuilder);
                result = Task<StreamUnwanted>.Run(() =>
                {
                    return data;
                });
            }
            else
            {
                this.throwErrorSystem(validation);
            }
            return await result;
        }

        public async Task createMany(List<StreamUnwanted> data)
        {
            // Jangan Lupa pastikan RuleSet ada!!
            var success = 0;
            foreach (var item in data)
            {
                var validation = this.validateModel(item, "StreamUnwantedInsertRuleSet");
                if (!validation.IsValid)
                {
                    this.throwErrorSystem(validation);
                    break;
                }
                success = 1;
            }
            if (success == 1)
            {
                await this.db.StreamUnwanted.InsertManyAsync(data);
            }

        }

        //TODO
        public Task updateMany(List<StreamUnwanted> data)
        {
            throw new NotImplementedException();
        }

        public async Task<StreamUnwanted> getById(string id)
        {
            var builderFilter = Builders<StreamUnwanted>.Filter;
            var filter = builderFilter.Eq(a => a.id, new ObjectId(id));

            Task<StreamUnwanted> result = null;
            using (var cursor = await this.db.StreamUnwanted.Find(filter).ToCursorAsync())
            {
                while (await cursor.MoveNextAsync())
                {
                    foreach (var doc in cursor.Current)
                    {
                        if (doc != null)
                        {
                            result = Task<StreamUnwanted>.Run(() =>
                            {
                                return doc;
                            });
                        }
                    }
                }
            }
            return await result;
        }

        public async Task<string> deleteById(string id)
        {
            var builderFilter = Builders<StreamUnwanted>.Filter.Eq(a => a.id, new ObjectId(id));
            await this.db.StreamUnwanted.FindOneAndDeleteAsync(builderFilter);
            Task<string> result = Task<string>.Run(() =>
            {
                return id;
            });
            return await result;
        }

        public async Task<StreamUnwanted[]> list(BsonDocument filter, BsonDocument sort, int skip, int limit)
        {

            Task<StreamUnwanted[]> result = null;
            List<StreamUnwanted> res = new List<StreamUnwanted>();
            using (var cursor = await this.db.StreamUnwanted.Find(filter).Sort(sort).Limit(limit).Skip(skip).ToCursorAsync())
            {
                while (await cursor.MoveNextAsync())
                {
                    foreach (var doc in cursor.Current)
                    {
                        res.Add(doc);
                    }
                }
            }

            result = Task<StreamUnwanted[]>.Run(() =>
            {
                return res.ToArray();
            });


            return await result;
        }

    }
}
