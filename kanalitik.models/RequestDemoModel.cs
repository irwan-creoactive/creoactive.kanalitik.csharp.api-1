﻿using kanalitik.data.access.documents;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace kanalitik.models
{


    public class RequestDemoModel : BaseModel<RequestDemo>, IBaseModel<RequestDemo>
    {

        public RequestDemoModel() : base("RequestDemoModel") { }

        public async Task<RequestDemo> createOne(RequestDemo data)
        {
            // Jangan Lupa pastikan RuleSet ada!!
            var validation = this.validateModel(data, "RequestDemoRegisterRuleSet");
            Task<RequestDemo> result = null;
            if (validation.IsValid)
            {
                await this.db.RequestDemo.InsertOneAsync(data);
                result = Task<RequestDemo>.Run(() =>
                {
                    return data;
                });
            }
            else
            {
                this.throwErrorSystem(validation);
            }
            return await result;
        }

        public async Task<RequestDemo> updateOne(RequestDemo data)
        {
            // Jangan Lupa pastikan RuleSet ada!!
            var validation = this.validateModel(data, "RequestDemoUpdateRuleSet");

            Task<RequestDemo> result = null;
            if (validation.IsValid)
            {
                var updateBuilder = new UpdateBuilderModel<RequestDemo>().fieldSet(data);
                var whereclause = new BsonDocument(new BsonElement("_id", data.id)).ToJson();
                await this.db.RequestDemo.FindOneAndUpdateAsync(whereclause, updateBuilder);
                result = Task<RequestDemo>.Run(() =>
                {
                    return data;
                });
            }
            else
            {
                this.throwErrorSystem(validation);
            }
            return await result;
        }

        public async Task createMany(List<RequestDemo> data)
        {
            // Jangan Lupa pastikan RuleSet ada!!
            var success = 0;
            foreach (var item in data)
            {
                var validation = this.validateModel(item, "RequestDemoRegisterRuleSet");
                if (!validation.IsValid)
                {
                    this.throwErrorSystem(validation);
                    break;
                }
                success = 1;
            }
            if (success == 1)
            {
                await this.db.RequestDemo.InsertManyAsync(data);
            }

        }

        //TODO
        public Task updateMany(List<RequestDemo> data)
        {
            throw new NotImplementedException();
        }

        public async Task<RequestDemo> getById(string id)
        {
            var builderFilter = Builders<RequestDemo>.Filter;
            var filter = builderFilter.Eq(a => a.id, new ObjectId(id));

            Task<RequestDemo> result = null;
            using (var cursor = await this.db.RequestDemo.Find(filter).ToCursorAsync())
            {
                while (await cursor.MoveNextAsync())
                {
                    foreach (var doc in cursor.Current)
                    {
                        if (doc != null)
                        {
                            result = Task<RequestDemo>.Run(() =>
                            {
                                return doc;
                            });
                        }
                    }
                }
            }
            return await result;
        }

        public async Task<string> deleteById(string id)
        {
            var builderFilter = Builders<RequestDemo>.Filter.Eq(a => a.id, new ObjectId(id));
            await this.db.RequestDemo.FindOneAndDeleteAsync(builderFilter);
            Task<string> result = Task<string>.Run(() =>
            {
                return id;
            });
            return await result;
        }

        public async Task<RequestDemo[]> list(BsonDocument filter, BsonDocument sort, int skip, int limit)
        {

            Task<RequestDemo[]> result = null;
            List<RequestDemo> res = new List<RequestDemo>();
            using (var cursor = await this.db.RequestDemo.Find(filter).Sort(sort).Limit(limit).Skip(skip).ToCursorAsync())
            {
                while (await cursor.MoveNextAsync())
                {
                    foreach (var doc in cursor.Current)
                    {
                        res.Add(doc);
                    }
                }
            }

            result = Task<RequestDemo[]>.Run(() =>
            {
                return res.ToArray();
            });


            return await result;
        }

    }
}
