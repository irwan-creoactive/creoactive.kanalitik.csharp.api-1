﻿using kanalitik.data.access.documents;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace kanalitik.models
{


    public class AccountSocialMediaModel : BaseModel<AccountSocialMedia>, IBaseModel<AccountSocialMedia>
    {

        public AccountSocialMediaModel() : base("AccountSocialMediaModel") { }

        public async Task<AccountSocialMedia> createOne(AccountSocialMedia data)
        {
            // Jangan Lupa pastikan RuleSet ada!!
            var validation = this.validateModel(data, "CreateRuleSet");
            Task<AccountSocialMedia> result = null;
            if (validation.IsValid)
            {
                await this.db.AccountSocialMedia.InsertOneAsync(data);
                result = Task<AccountSocialMedia>.Run(() =>
                {
                    return data;
                });
            }
            else
            {
                this.throwErrorSystem(validation);
            }
            return await result;
        }

        public async Task<AccountSocialMedia> updateOne(AccountSocialMedia data)
        {
            // Jangan Lupa pastikan RuleSet ada!!
            var validation = this.validateModel(data, "UpdateRuleSet");

            Task<AccountSocialMedia> result = null;
            if (validation.IsValid)
            {
                var updateBuilder = new UpdateBuilderModel<AccountSocialMedia>().fieldSet(data);
                var whereclause = new BsonDocument(new BsonElement("_id", data.id)).ToJson();
                await this.db.AccountSocialMedia.FindOneAndUpdateAsync(whereclause, updateBuilder);
                result = Task<AccountSocialMedia>.Run(() =>
                {
                    return data;
                });
            }
            else
            {
                this.throwErrorSystem(validation);
            }
            return await result;
        }

        public async Task createMany(List<AccountSocialMedia> data)
        {
            // Jangan Lupa pastikan RuleSet ada!!
            var success = 0;
            foreach (var item in data)
            {
                var validation = this.validateModel(item, "CreateRuleSet");
                if (!validation.IsValid)
                {
                    this.throwErrorSystem(validation);
                    break;
                }
                success = 1;
            }
            if (success == 1)
            {
                await this.db.AccountSocialMedia.InsertManyAsync(data);
            }

        }

        //TODO
        public Task updateMany(List<AccountSocialMedia> data)
        {
            throw new NotImplementedException();
        }

        public async Task<AccountSocialMedia> getById(string id)
        {
            var builderFilter = Builders<AccountSocialMedia>.Filter;
            var filter = builderFilter.Eq(a => a.id, new ObjectId(id));

            Task<AccountSocialMedia> result = null;
            using (var cursor = await this.db.AccountSocialMedia.Find(filter).ToCursorAsync())
            {
                while (await cursor.MoveNextAsync())
                {
                    foreach (var doc in cursor.Current)
                    {
                        if (doc != null)
                        {
                            result = Task<AccountSocialMedia>.Run(() =>
                            {
                                return doc;
                            });
                        }
                    }
                }
            }
            return await result;
        }

        public async Task<string> deleteById(string id)
        {
            var builderFilter = Builders<AccountSocialMedia>.Filter.Eq(a => a.id, new ObjectId(id));
            await this.db.AccountSocialMedia.FindOneAndDeleteAsync(builderFilter);
            Task<string> result = Task<string>.Run(() =>
            {
                return id;
            });
            return await result;
        }

        public async Task<AccountSocialMedia[]> list(BsonDocument filter, BsonDocument sort, int skip, int limit)
        {

            Task<AccountSocialMedia[]> result = null;
            List<AccountSocialMedia> res = new List<AccountSocialMedia>();
            using (var cursor = await this.db.AccountSocialMedia.Find(filter).Sort(sort).Limit(limit).Skip(skip).ToCursorAsync())
            {
                while (await cursor.MoveNextAsync())
                {
                    foreach (var doc in cursor.Current)
                    {
                        res.Add(doc);
                    }
                }
            }

            result = Task<AccountSocialMedia[]>.Run(() =>
            {
                return res.ToArray();
            });


            return await result;
        }

    }
}
