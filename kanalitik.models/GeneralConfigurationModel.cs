﻿using kanalitik.data.access.documents;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace kanalitik.models
{


    public class GeneralConfigurationModel : BaseModel<GeneralConfiguration>, IBaseModel<GeneralConfiguration>
    {

        public GeneralConfigurationModel() : base("GeneralConfigurationModel") { }

        public async Task<GeneralConfiguration> createOne(GeneralConfiguration data)
        {
            // Jangan Lupa pastikan RuleSet ada!!
            var validation = this.validateModel(data, "GeneralConfigurationInsertRuleSet");
            Task<GeneralConfiguration> result = null;
            if (validation.IsValid)
            {
                await this.db.GeneralConfiguration.InsertOneAsync(data);
                result = Task<GeneralConfiguration>.Run(() =>
                {
                    return data;
                });
            }
            else
            {
                this.throwErrorSystem(validation);
            }
            return await result;
        }

        public async Task<GeneralConfiguration> updateOne(GeneralConfiguration data)
        {
            // Jangan Lupa pastikan RuleSet ada!!
            var validation = this.validateModel(data, "GeneralConfigurationUpdateRuleSet");

            Task<GeneralConfiguration> result = null;
            if (validation.IsValid)
            {
                var updateBuilder = new UpdateBuilderModel<GeneralConfiguration>().fieldSet(data);
                var whereclause = new BsonDocument(new BsonElement("_id", data.id)).ToJson();
                await this.db.GeneralConfiguration.FindOneAndUpdateAsync(whereclause, updateBuilder);
                result = Task<GeneralConfiguration>.Run(() =>
                {
                    return data;
                });
            }
            else
            {
                this.throwErrorSystem(validation);
            }
            return await result;
        }

        public async Task createMany(List<GeneralConfiguration> data)
        {
            // Jangan Lupa pastikan RuleSet ada!!
            var success = 0;
            foreach (var item in data)
            {
                var validation = this.validateModel(item, "CreateRuleSet");
                if (!validation.IsValid)
                {
                    this.throwErrorSystem(validation);
                    break;
                }
                success = 1;
            }
            if (success == 1)
            {
                await this.db.GeneralConfiguration.InsertManyAsync(data);
            }

        }

        //TODO
        public Task updateMany(List<GeneralConfiguration> data)
        {
            throw new NotImplementedException();
        }

        public async Task<GeneralConfiguration> getById(string id)
        {
            var builderFilter = Builders<GeneralConfiguration>.Filter;
            var filter = builderFilter.Eq(a => a.id, new ObjectId(id));

            Task<GeneralConfiguration> result = null;
            using (var cursor = await this.db.GeneralConfiguration.Find(filter).ToCursorAsync())
            {
                while (await cursor.MoveNextAsync())
                {
                    foreach (var doc in cursor.Current)
                    {
                        if (doc != null)
                        {
                            result = Task<GeneralConfiguration>.Run(() =>
                            {
                                return doc;
                            });
                        }
                    }
                }
            }
            return await result;
        }

        public async Task<string> deleteById(string id)
        {
            var builderFilter = Builders<GeneralConfiguration>.Filter.Eq(a => a.id, new ObjectId(id));
            await this.db.GeneralConfiguration.FindOneAndDeleteAsync(builderFilter);
            Task<string> result = Task<string>.Run(() =>
            {
                return id;
            });
            return await result;
        }

        public async Task<GeneralConfiguration[]> list(BsonDocument filter, BsonDocument sort, int skip, int limit)
        {

            Task<GeneralConfiguration[]> result = null;
            List<GeneralConfiguration> res = new List<GeneralConfiguration>();
            using (var cursor = await this.db.GeneralConfiguration.Find(filter).Sort(sort).Limit(limit).Skip(skip).ToCursorAsync())
            {
                while (await cursor.MoveNextAsync())
                {
                    foreach (var doc in cursor.Current)
                    {
                        res.Add(doc);
                    }
                }
            }

            result = Task<GeneralConfiguration[]>.Run(() =>
            {
                return res.ToArray();
            });


            return await result;
        }

    }
}
