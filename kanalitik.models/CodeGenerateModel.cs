﻿using kanalitik.data.access.documents;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace kanalitik.models
{


    public class CodeGenerateModel : BaseModel<CodeGenerate>, IBaseModel<CodeGenerate>
    {

        public CodeGenerateModel() : base("CodeGenerateModel") { }

        public async Task<CodeGenerate> createOne(CodeGenerate data)
        {
            // Jangan Lupa pastikan RuleSet ada!!
            var validation = this.validateModel(data, "CreateRuleSet");
            Task<CodeGenerate> result = null;
            if (validation.IsValid)
            {
                await this.db.CodeGenerate.InsertOneAsync(data);
                result = Task<CodeGenerate>.Run(() =>
                {
                    return data;
                });
            }
            else
            {
                this.throwErrorSystem(validation);
            }
            return await result;
        }

        public async Task<CodeGenerate> updateOne(CodeGenerate data)
        {
            // Jangan Lupa pastikan RuleSet ada!!
            var validation = this.validateModel(data, "UpdateRuleSet");

            Task<CodeGenerate> result = null;
            if (validation.IsValid)
            {
                var updateBuilder = new UpdateBuilderModel<CodeGenerate>().fieldSet(data);
                var whereclause = new BsonDocument(new BsonElement("_id", data.id)).ToJson();
                await this.db.CodeGenerate.FindOneAndUpdateAsync(whereclause, updateBuilder);
                result = Task<CodeGenerate>.Run(() =>
                {
                    return data;
                });
            }
            else
            {
                this.throwErrorSystem(validation);
            }
            return await result;
        }

        public async Task createMany(List<CodeGenerate> data)
        {
            // Jangan Lupa pastikan RuleSet ada!!
            var success = 0;
            foreach (var item in data)
            {
                var validation = this.validateModel(item, "CreateRuleSet");
                if (!validation.IsValid)
                {
                    this.throwErrorSystem(validation);
                    break;
                }
                success = 1;
            }
            if (success == 1)
            {
                await this.db.CodeGenerate.InsertManyAsync(data);
            }

        }

        //TODO
        public Task updateMany(List<CodeGenerate> data)
        {
            throw new NotImplementedException();
        }

        public async Task<CodeGenerate> getById(string id)
        {
            var builderFilter = Builders<CodeGenerate>.Filter;
            var filter = builderFilter.Eq(a => a.id, new ObjectId(id));

            Task<CodeGenerate> result = null;
            using (var cursor = await this.db.CodeGenerate.Find(filter).ToCursorAsync())
            {
                while (await cursor.MoveNextAsync())
                {
                    foreach (var doc in cursor.Current)
                    {
                        if (doc != null)
                        {
                            result = Task<CodeGenerate>.Run(() =>
                            {
                                return doc;
                            });
                        }
                    }
                }
            }
            return await result;
        }

        public async Task<string> deleteById(string id)
        {
            var builderFilter = Builders<CodeGenerate>.Filter.Eq(a => a.id, new ObjectId(id));
            await this.db.CodeGenerate.FindOneAndDeleteAsync(builderFilter);
            Task<string> result = Task<string>.Run(() =>
            {
                return id;
            });
            return await result;
        }

        public async Task<CodeGenerate[]> list(BsonDocument filter, BsonDocument sort, int skip, int limit)
        {

            Task<CodeGenerate[]> result = null;
            List<CodeGenerate> res = new List<CodeGenerate>();
            using (var cursor = await this.db.CodeGenerate.Find(filter).Sort(sort).Limit(limit).Skip(skip).ToCursorAsync())
            {
                while (await cursor.MoveNextAsync())
                {
                    foreach (var doc in cursor.Current)
                    {
                        res.Add(doc);
                    }
                }
            }

            result = Task<CodeGenerate[]>.Run(() =>
            {
                return res.ToArray();
            });


            return await result;
        }

    }
}
