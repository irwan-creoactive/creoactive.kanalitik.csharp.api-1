﻿using kanalitik.data.access.documents;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace kanalitik.models
{


    public class PackageModel : BaseModel<Package>, IBaseModel<Package>
    {

        public PackageModel() : base("PackageModel") { }

        public async Task<Package> createOne(Package data)
        {
            // Jangan Lupa pastikan RuleSet ada!!
            var validation = this.validateModel(data, "PackageCreateRuleSet");
            Task<Package> result = null;
            if (validation.IsValid)
            {
                await this.db.Package.InsertOneAsync(data);
                result = Task<Package>.Run(() =>
                {
                    return data;
                });
            }
            else
            {
                this.throwErrorSystem(validation);
            }
            return await result;
        }

        public async Task<Package> updateOne(Package data)
        {
            // Jangan Lupa pastikan RuleSet ada!!
            var validation = this.validateModel(data, "PackageUpdateRuleSet");

            Task<Package> result = null;
            if (validation.IsValid)
            {
                var updateBuilder = new UpdateBuilderModel<Package>().fieldSet(data);
                var whereclause = new BsonDocument(new BsonElement("_id", data.id)).ToJson();
                await this.db.Package.FindOneAndUpdateAsync(whereclause, updateBuilder);
                result = Task<Package>.Run(() =>
                {
                    return data;
                });
            }
            else
            {
                this.throwErrorSystem(validation);
            }
            return await result;
        }

        public async Task createMany(List<Package> data)
        {
            // Jangan Lupa pastikan RuleSet ada!!
            var success = 0;
            foreach (var item in data)
            {
                var validation = this.validateModel(item, "PackageCreateRuleSet");
                if (!validation.IsValid)
                {
                    this.throwErrorSystem(validation);
                    break;
                }
                success = 1;
            }
            if (success == 1)
            {
                await this.db.Package.InsertManyAsync(data);
            }

        }

        //TODO
        public Task updateMany(List<Package> data)
        {
            throw new NotImplementedException();
        }

        public async Task<Package> getById(string id)
        {
            var builderFilter = Builders<Package>.Filter;
            var filter = builderFilter.Eq(a => a.id, new ObjectId(id));

            Task<Package> result = null;
            using (var cursor = await this.db.Package.Find(filter).ToCursorAsync())
            {
                while (await cursor.MoveNextAsync())
                {
                    foreach (var doc in cursor.Current)
                    {
                        if (doc != null)
                        {
                            result = Task<Package>.Run(() =>
                            {
                                return doc;
                            });
                        }
                    }
                }
            }
            return await result;
        }

        public async Task<string> deleteById(string id)
        {
            var builderFilter = Builders<Package>.Filter.Eq(a => a.id, new ObjectId(id));
            await this.db.Package.FindOneAndDeleteAsync(builderFilter);
            Task<string> result = Task<string>.Run(() =>
            {
                return id;
            });
            return await result;
        }

        public async Task<Package[]> list(BsonDocument filter, BsonDocument sort, int skip, int limit)
        {

            Task<Package[]> result = null;
            List<Package> res = new List<Package>();
            using (var cursor = await this.db.Package.Find(filter).Sort(sort).Limit(limit).Skip(skip).ToCursorAsync())
            {
                while (await cursor.MoveNextAsync())
                {
                    foreach (var doc in cursor.Current)
                    {
                        res.Add(doc);
                    }
                }
            }

            result = Task<Package[]>.Run(() =>
            {
                return res.ToArray();
            });


            return await result;
        }

    }
}
