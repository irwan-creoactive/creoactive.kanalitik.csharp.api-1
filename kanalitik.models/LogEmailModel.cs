﻿using kanalitik.data.access.documents;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace kanalitik.models
{


    public class LogEmailModel : BaseModel<LogEmail>, IBaseModel<LogEmail>
    {

        public LogEmailModel() : base("LogEmailModel") { }

        public async Task<LogEmail> createOne(LogEmail data)
        {
            // Jangan Lupa pastikan RuleSet ada!!
            var validation = this.validateModel(data, "LogEmailInsertRuleSet");
            Task<LogEmail> result = null;
            if (validation.IsValid)
            {
                await this.db.LogEmail.InsertOneAsync(data);
                result = Task<LogEmail>.Run(() =>
                {
                    return data;
                });
            }
            else
            {
                this.throwErrorSystem(validation);
            }
            return await result;
        }

        public async Task<LogEmail> updateOne(LogEmail data)
        {
            // Jangan Lupa pastikan RuleSet ada!!
            var validation = this.validateModel(data, "LogEmailUpdateRuleSet");

            Task<LogEmail> result = null;
            if (validation.IsValid)
            {
                var updateBuilder = new UpdateBuilderModel<LogEmail>().fieldSet(data);
                var whereclause = new BsonDocument(new BsonElement("_id", data.id)).ToJson();
                await this.db.LogEmail.FindOneAndUpdateAsync(whereclause, updateBuilder);
                result = Task<LogEmail>.Run(() =>
                {
                    return data;
                });
            }
            else
            {
                this.throwErrorSystem(validation);
            }
            return await result;
        }

        public async Task createMany(List<LogEmail> data)
        {
            // Jangan Lupa pastikan RuleSet ada!!
            var success = 0;
            foreach (var item in data)
            {
                var validation = this.validateModel(item, "LogEmailInsertRuleSet");
                if (!validation.IsValid)
                {
                    this.throwErrorSystem(validation);
                    break;
                }
                success = 1;
            }
            if (success == 1)
            {
                await this.db.LogEmail.InsertManyAsync(data);
            }

        }

        //TODO
        public Task updateMany(List<LogEmail> data)
        {
            throw new NotImplementedException();
        }

        public async Task<LogEmail> getById(string id)
        {
            var builderFilter = Builders<LogEmail>.Filter;
            var filter = builderFilter.Eq(a => a.id, new ObjectId(id));

            Task<LogEmail> result = null;
            using (var cursor = await this.db.LogEmail.Find(filter).ToCursorAsync())
            {
                while (await cursor.MoveNextAsync())
                {
                    foreach (var doc in cursor.Current)
                    {
                        if (doc != null)
                        {
                            result = Task<LogEmail>.Run(() =>
                            {
                                return doc;
                            });
                        }
                    }
                }
            }
            return await result;
        }

        public async Task<string> deleteById(string id)
        {
            var builderFilter = Builders<LogEmail>.Filter.Eq(a => a.id, new ObjectId(id));
            await this.db.LogEmail.FindOneAndDeleteAsync(builderFilter);
            Task<string> result = Task<string>.Run(() =>
            {
                return id;
            });
            return await result;
        }

        public async Task<LogEmail[]> list(BsonDocument filter, BsonDocument sort, int skip, int limit)
        {

            Task<LogEmail[]> result = null;
            List<LogEmail> res = new List<LogEmail>();
            using (var cursor = await this.db.LogEmail.Find(filter).Sort(sort).Limit(limit).Skip(skip).ToCursorAsync())
            {
                while (await cursor.MoveNextAsync())
                {
                    foreach (var doc in cursor.Current)
                    {
                        res.Add(doc);
                    }
                }
            }

            result = Task<LogEmail[]>.Run(() =>
            {
                return res.ToArray();
            });


            return await result;
        }


    }
}
