﻿using kanalitik.data.contract;
using kanalitik.data.contract.apiRequest;
using kanalitik.data.contract.apiResponse;
using kanalitik.request.blog;
using kanalitik.request.socialmedia;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kanalitik.interfaces
{
    public interface IBlogService
    {
        /// <summary>
        /// Membuat document baru pada collection blog
        /// 
        /// proses : 
        ///     blog.title = request.Title
        ///     blog.content = request.Content
        ///     blog.image = request.Image
        ///     blog.is_publish = request.IsPublish
        /// </summary>
        /// <param name="request">Berisi Title, Content, Image, IsPublish</param>
        /// <returns>blog._id</returns>
        ActorResponse<BlogResponse> createBlog(ActorRequest<BlogRequest> request);

        /// <summary>
        /// Merubah document pada collection blog
        /// 
        /// proses : 
        ///     blog.title = request.Title
        ///     blog.content = request.Content
        ///     blog.image = request.Image
        ///     blog.is_publish = request.IsPublish
        ///     
        ///     dengan syarat blog._id = request._id
        /// </summary>
        /// <param name="request">Berisi _id, Title, Content, Image, IsPublish</param>
        /// <returns>blog._id</returns>
        ActorResponse<BlogResponse> updateBlog(ActorRequest<BlogRequest> request);

        /// <summary>
        /// Merubah isian blog.is_publish = true
        /// 
        /// proses :
        ///     blog.is_publish = true
        ///     
        ///     dengan syarat blog._id = request._id
        /// </summary>
        /// <param name="request">berisi _id</param>
        /// <returns>blog._id</returns>
        ActorResponse<BlogResponse> publishBlog(ActorRequest<BlogRequest> request);

        /// <summary>
        /// Merubah isian blog.is_publish = false
        /// 
        /// proses :
        ///     blog.is_publish = false
        ///     
        ///     dengan syarat blog._id = request._id
        /// </summary>
        /// <param name="request">berisi _id</param>
        /// <returns>blog._id</returns>
        ActorResponse<BlogResponse> unpublishBlog(ActorRequest<BlogRequest> request);

        /// <summary>
        /// Menampilkan list collection blog dengan filter blog.is_publish = true
        /// Dengan filter blog.comment.is_approve = true
        /// </summary>
        /// <param></param>
        /// <returns>List Blog</returns>
        ActorResponse<ListBlogResponse> viewListPublishBlog();

        /// <summary>
        /// Menampilkan seluruh list collection blog.
        /// Tanpa filter comment.
        /// </summary>
        /// <param></param>
        /// <returns>List Blog</returns>
        ActorResponse<ListBlogResponse> viewAllListBlog();

        /// <summary>
        /// Menampilkan collection blog dengan filter blog._id = request._id
        /// Dengan filter blog.comment.is_approve = true
        /// </summary>
        /// <param name="request">berisi _id</param>
        /// <returns>Blog Detail</returns>
        ActorResponse<BlogResponse> viewDetailBlog(ActorRequest<BlogRequest> request);

        /// <summary>
        /// Menambahkan comment pada blog-id tertentu.
        /// 
        /// proses : 
        ///     add blog.seq diisi counter (bertambah terus)
        ///     add blog.email = request.Email
        ///     add blog.name = request.Name
        ///     add blog.comment = request.Comment
        ///     add blog.is_approve diisi default false atau request.IsApprove
        /// </summary>
        /// <param name="request">berisi BlogId, Email, Name, Comment, IsApprove</param>
        /// <returns>BlogId, Seq</returns>
        ActorResponse<BlogCommentResponse> insertBlogComment(ActorRequest<BlogCommentRequest> request);

        /// <summary>
        /// Merubah comment.is_approve menjadi true
        /// 
        /// proses : 
        ///     edit blog.is_approve = true
        ///         syarat :
        ///             blog._id = request.BlogId
        ///             blog.comment.seq = request.Seq
        /// </summary>
        /// <param name="request">berisi BlogId, Seq</param>
        /// <returns>BlogId, Seq</returns>
        ActorResponse<BlogCommentResponse> approveBlogComment(ActorRequest<BlogCommentRequest> request);

    }
}
