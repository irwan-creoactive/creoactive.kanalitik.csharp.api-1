﻿using kanalitik.data.contract;
using kanalitik.data.contract.apiRequest;
using kanalitik.data.contract.apiResponse;
using kanalitik.request.user;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kanalitik.interfaces
{
    public interface IUserService
    {
        ActorResponse<BaseResponse> checkAccountIsExist(ActorRequest<string> request);

        ActorResponse<RegisterResponse> register(ActorRequest<RegisterRequest> user);

        ActorResponse<UserProfileResponse> getProfile(ActorRequest<BaseRequest> req);

        ActorResponse<BaseResponse> saveProfile(ActorRequest<BaseRequest> req);
    }
}
