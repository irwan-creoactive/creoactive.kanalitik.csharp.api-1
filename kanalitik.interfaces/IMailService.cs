﻿using kanalitik.data.contract;
using kanalitik.data.contract.apiRequest;
using kanalitik.data.contract.apiResponse;
using kanalitik.request.mail;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kanalitik.interfaces
{
    public interface IMailService
    {
        /// <summary>
        /// untuk menjalankan Mail service
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        ActorResponse<BaseResponse> serviceRun(ActorRequest<BaseRequest> form);

        /// <summary>
        /// create update mail log untuk kemudian akan dikirimkan oleh service mail
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        ActorResponse<MailResponse> mailSave(ActorRequest<MailSaveRequest> form);

        /// <summary>
        /// read mail from log
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        ActorResponse<MailResponse> mailList(ActorRequest<MailRequest> form);

    }
}
