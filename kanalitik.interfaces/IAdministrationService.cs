﻿using kanalitik.data.contract;
using kanalitik.data.contract.apiRequest;
using kanalitik.data.contract.apiResponse;
using kanalitik.request.user;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kanalitik.interfaces
{
    public interface IAdministrationService
    {
        /// <summary>
        /// Memeriksa apakah User sudah memiliki package?
        /// 
        /// Proses : 
        ///     Periksa apakah user.role_user sudah terisi?
        ///     Jika sudah terisi, return true; 
        ///     Jika belum terisi (null), return false;
        /// 
        /// </summary>
        /// <param name="request">string object-id dari user</param>
        /// <returns>true-false</returns>
        ActorResponse<BaseResponse> checkPackageIsExist(ActorRequest<string> request);

        /// <summary>
        /// Mengambil data yang package untuk ditampilkan di depan.
        ///
        /// Proses : 
        ///     Mengambil data dari collection package dengan ketentuan berikut :
        ///     package.is_show berisi true
        ///     package.social_media.is_show berisi true
        ///     package.blog.is_show berisi true
        ///     package.news_portal.is_show berisi true
        /// 
        /// </summary>
        /// <param></param>
        /// <returns>List<PackageListResponse></returns>
        ActorResponse<List<PackageListResponse>> showPackageList();

        /// <summary>
        /// Mengambil data yang package untuk ditampilkan di depan. tidak
        /// 
        /// Proses : 
        ///     Mengambil data dari collection package dengan ketentuan berikut :
        ///     package.is_show berisi true
        /// 
        /// </summary>
        /// <param name="request">string object-id dari package</param>
        /// <returns>List<PackageListResponse></returns>
        ActorResponse<List<PackageListResponse>> showPackageDetailList(ActorRequest<string> request);

        /// <summary>
        /// Order package dilakukan user; sistem mencatatkan pada collection package_order.
        /// 
        /// Proses : 
        ///     buatkan invoice terkait pemesanan ini.
        ///     buatkan data pada collection package_order.
        ///     package_id diisi request.PackageId
        ///     user_id diisi request.UserId
        ///     order_status diisi open
        ///     client_id diisi null
        ///     status diisi unpaid
        ///     activate_status diisi false
        ///     invoice diisi nomor invoice yang sudah dibuat sebelumnya
        ///     expired_at diisi tanggal batas akhir pembayaran invoice. 
        ///     tanggal pembuatan data ditambah tenggat hari invoice. 
        ///     Tenggat hari invoice diambil dari collection general_configuration.
        /// 
        /// </summary>
        /// <param name="request">object berisi package-id dan user-id</param>
        /// <returns>object-id dari collection package_order</returns>
        ActorResponse<OrderPackageResponse> orderPackage(ActorRequest<OrderPackageRequest> request);

        /// <summary>
        /// user melakukan order paket yang diminati
        /// 
        /// fahmi note : fungsi ini kayaknya sama dengan orderPackage; Beda dimana?
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        ActorResponse<OrderResponse> packageOrder(ActorRequest<OrderRequest> form);

        /// <summary>
        /// admin melakukan aktivasi paket yang diminta user setelah bayar.
        /// 
        /// update package_order :
        ///     order_status : close
        ///     status : paid
        ///     activate_status : true
        /// 
        /// membuat data client dengan name di isi user_business_name
        ///     jika user_business_type adalah organization atau company
        ///     jika user_business_type berisi personal maka client_name di isi nama user
        /// 
        /// Ketika proses aktifasi selesai, sistem mengirimkan 2 email pada user yang diaktifkan
        /// 
        /// 1. Email bahwa kanalitik sudah menerima pembayaran.
        /// 2. Email bahwa package sudah aktif.
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        ActorResponse<PackageActivationResponse> packageActivation(ActorRequest<PackageActivationRequest> form);

        /// <summary>
        /// create update delete package
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        ActorResponse<PackageResponse> packageSave(ActorRequest<PackageRequest> form);

        /// <summary>
        /// read package
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        ActorResponse<PackageResponse> packageList(ActorRequest<PackageRequest> form);

        /// <summary>
        /// Memeriksa apakah package yang dipilih adalah package yang dimiliki client?
        /// 
        /// Proses : 
        ///     bandingkan client.package.id dengan package.id
        ///     jika sama, return true;
        ///     beda, return false;
        /// 
        /// </summary>
        /// <param name="package_compare">berisi package_id dan client_package_id</param>
        /// <returns>true-false</returns>
        ActorResponse<bool> isPackageClient(ActorRequest<PackageCompareRequest> package_compare);

        /// <summary>
        /// proses untuk memperpanjang cycling package yang dipesan
        /// 
        /// Proses : 
        ///     buatkan invoice terkait pemesanan ini.
        ///     buatkan data pada collection package_order.
        ///     package_id diisi request.PackageId
        ///     user_id diisi request.UserId
        ///     order_status diisi open
        ///     order_type diisi renew
        ///     client_id diisi request.ClientId
        ///     status diisi unpaid
        ///     activate_status : false
        ///     invoice diisi nomor invoice yang sudah dibuat sebelumnya
        ///     expired_at diisi tanggal batas akhir pembayaran invoice. 
        ///     tanggal pembuatan data ditambah tenggat hari invoice. 
        ///     Tenggat hari invoice diambil dari collection general_configuration.
        /// 
        /// </summary>
        /// <param name="request">object berisi package-id, user-id dan client-id</param>
        /// <returns>object-id dari collection package_order</returns>
        ActorResponse<OrderPackageResponse> renewPackage(ActorRequest<OrderPackageRequest> request);

        /// <summary>
        /// admin melakukan approve renew cycling package.
        /// 
        /// Proses :
        /// update package_order :
        ///     order_status : close
        ///     status : paid
        ///     activate_status : true
        ///     
        /// update client.package.expired_at ditambah 1 cycling period.
        /// 
        /// 1. Email bahwa kanalitik sudah menerima pembayaran renew.
        /// 2. Email bahwa package sudah diperbaharui cycling periodnya.
        /// </summary>
        /// <param name="request">client-id</param>
        /// <returns>true-false</returns>
        ActorResponse<bool> approveRenewPackage(ActorRequest<string> request);

        /// <summary>
        /// proses untuk mengganti package ke package yang lebih rendah kelasnya pada next billing cycle.
        /// 
        /// Proses : 
        ///     buatkan invoice terkait pemesanan ini.
        ///     buatkan data pada collection package_order.
        ///     package_id diisi request.PackageId
        ///     user_id diisi request.UserId
        ///     order_status diisi open
        ///     order_type diisi downgrade
        ///     order_type_start diisi request.OrderTypeStart
        ///     client_id diisi request.ClientId
        ///     status diisi unpaid
        ///     activate_status : false
        ///     invoice diisi nomor invoice yang sudah dibuat sebelumnya
        ///     expired_at diisi tanggal batas akhir pembayaran invoice. 
        ///     tanggal pembuatan data ditambah tenggat hari invoice. 
        ///     Tenggat hari invoice diambil dari collection general_configuration.
        /// 
        /// </summary>
        /// <param name="request">object berisi package-id, user-id dan client-id</param>
        /// <returns>object-id dari collection package_order</returns>
        ActorResponse<OrderPackageResponse> downgradePackage(ActorRequest<OrderPackageRequest> request);

        /// <summary>
        /// proses untuk mengganti package ke package yang lebih rendah kelasnya pada next billing cycle.
        /// 
        /// Proses : 
        ///     buatkan invoice terkait pemesanan ini.
        ///     periksa jika tidak ada invoice, hitung sisa lebih expired date package.
        ///     expire date package ini nanti akan dimasukkan pada collection package_order.order_type_end
        ///     
        ///     buatkan data pada collection package_order.
        ///         package_id diisi request.PackageId
        ///         user_id diisi request.UserId
        ///         order_status diisi open
        ///         order_type diisi upgrade
        ///         order_type_start diisi request.OrderTypeStart
        ///         order_type_end diisi new expired date
        ///         client_id diisi request.ClientId
        ///         status diisi unpaid
        ///         activate_status : false
        ///         invoice diisi nomor invoice yang sudah dibuat sebelumnya
        ///         expired_at diisi tanggal batas akhir pembayaran invoice. 
        ///         tanggal pembuatan data ditambah tenggat hari invoice. 
        ///         Tenggat hari invoice diambil dari collection general_configuration.
        /// 
        /// </summary>
        /// <param name="request">object berisi package-id, user-id dan client-id</param>
        /// <returns>object-id dari collection package_order</returns>
        ActorResponse<OrderPackageResponse> upgradePackage(ActorRequest<OrderPackageRequest> request);

        /// <summary>
        /// admin melakukan approve renew cycling package.
        /// 
        /// Proses :
        /// update package_order :
        ///     order_status : close
        ///     status : paid
        ///     activate_status : false
        ///     
        /// 1. Email bahwa kanalitik sudah menerima pembayaran renew.
        /// 2. Email bahwa package akan berubah pada next billing cycle
        /// 
        /// Buat service untuk memeriksa upgrade/downgrade package pada setiap akhir periode cycling.
        /// </summary>
        /// <param name="request">string package_order</param>
        /// <returns>true-false</returns>
        ActorResponse<bool> approveUpDownGradePackage(ActorRequest<string> request);

        /// <summary>
        /// Memeriksa apakah user id ini adalah owner dari sebuah client?
        /// 
        /// Proses : 
        ///     banding antara request.UserId dengan client dengan client.owned_user dimana client_id = request.ClientId
        /// 
        /// </summary>
        /// <param name="request">UserId dan ClientId</param>
        /// <returns>true-false</returns>
        ActorResponse<bool> isUserOwner(ActorRequest<UserOwnerRequest> request);

        /// <summary>
        /// Menonaktifkan paket yang saat ini sedang dimiliki oleh client.
        /// 
        /// Proses : 
        ///     buatkan invoice terkait pemesanan ini.
        ///     buatkan data pada collection package_order.
        ///         package_id diisi client.package._id (diambil dari data client dengan param request.ClientId)
        ///         user_id diisi request.UserId
        ///         order_status diisi close
        ///         order_type diisi stop
        ///         client_id diisi request.ClientId
        ///         status diisi unpaid
        ///         activate_status : true
        ///         invoice diisi nomor invoice yang sudah dibuat sebelumnya
        ///         expired_at diisi tanggal batas akhir pembayaran invoice. 
        ///         tanggal pembuatan data ditambah tenggat hari invoice. 
        ///         Tenggat hari invoice diambil dari collection general_configuration.
        ///         
        ///     update data client
        ///         client.status = nonactive
        /// 
        /// </summary>
        /// <param name="request">string object-id dari client-id</param>
        /// <returns>true-false</returns>
        ActorResponse<bool> stopSubscribe(ActorRequest<StopSubscribeRequest> request);

        /// <summary>
        /// Change data client & user
        /// 
        /// Proses : 
        ///     update data pada collection client.
        ///         client.name = request.ClientName
        ///         
        ///     update data user
        ///         user.business_type = request.BusinessType
        /// 
        /// </summary>
        /// <param name="request">string ClientId, ClientName, UserId, BusinessType</param>
        /// <returns>string ClientID, UserId</returns>
        ActorResponse<ClientSettingResponse> clientSetting(ActorRequest<ClientSettingRequestApi> request);

        /// <summary>
        /// Change data user
        /// 
        /// Proses : 
        ///     update data pada collection user.
        ///         user.name = request.Name
        ///         user.email = request.Email
        ///         user.password = request.Password
        ///         user.contact_number = request.ContactNumber
        /// 
        /// </summary>
        /// <param name="request">string ClientId, ClientName, UserId, BusinessType</param>
        /// <returns>string ClientID, UserId</returns>
        ActorResponse<AccountSettingResponse> accountSetting(ActorRequest<AccountSettingRequest> request);

        /// <summary>
        /// Invite new team member
        /// 
        /// Proses : 
        ///     create data user 
        ///         user.name = request.Name
        ///         user.email = request.Email
        ///         user.client.client_id = request.ClientId
        ///         user.role_user = request.RoleUser
        ///             hanya boleh berisi 
        ///                 3 - Account Administrator
        ///                 5 - Admin Head
        ///                 6 - Basic Admin
        ///     
        ///     create code_generate
        ///         code_generate.user = user_id yang tadi dibuat
        ///         type = invitation
        ///         code = generate code for invitation
        ///         
        ///     Sent email invitation based on code generated
        ///         
        ///     add data collection client
        ///         client.member.user_id = user_id yang tadi dibuat
        ///         client.member.role_user = request.RoleUser
        ///         client.member.status = nonactive
        /// 
        /// </summary>
        /// <param name="request">string Name, Email, ClientId, RoleUser</param>
        /// <returns>string ClientID</returns>
        ActorResponse<InviteClientResponse> inviteMemberClient(ActorRequest<InviteClientRequest> request);

        /// <summary>
        /// edit team member dalam satu client;
        /// 
        /// Proses : 
        ///     update client.member.role_user = request.RoleUser
        ///         dimana client.member.user_id = request.UserId
        /// 
        /// </summary>
        /// <param name="request">string UserId, RoleUser, ClientId</param>
        /// <returns>string ClientID</returns>
        ActorResponse<InviteClientResponse> editMemberClient(ActorRequest<MemberClientRequest> request);

        /// <summary>
        /// delete team member dalam satu client
        /// 
        /// Proses : 
        ///     delete client.member
        ///         dimana client.member.user_id = request.UserId
        /// 
        /// </summary>
        /// <param name="request">string UserId, RoleUser, ClientId</param>
        /// <returns>string ClientID</returns>
        ActorResponse<InviteClientResponse> deleteMemberClient(ActorRequest<MemberClientRequest> request);




    }
}
