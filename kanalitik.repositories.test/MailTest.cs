﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Practices.Unity;
using kanalitik.interfaces;

namespace kanalitik.repositories.test
{
    [TestClass]
    public class MailTest
    {

        [TestMethod]
        public void saveTest()
        {
            var container = new UnityContainer();
            container.RegisterType<IMailService, MailRepository>();
            var svc = container.Resolve<IMailService>();

            var req = new kanalitik.data.contract.ActorRequest<kanalitik.request.mail.MailSaveRequest>();
            req.data = new request.mail.MailSaveRequest();
            // req.data.CultureName = "id-ID";
            req.data.CultureName = "en-US";
            req.data.email = "asd@asd.com";
            req.data.configCode = "registration-notify";

            req.data.UserId = req.data.email;

            var actual = svc.mailSave(req);
            Assert.AreEqual(true, actual.Result.success);

        }
    }
}
