﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using kanalitik.models;
using kanalitik.data.access.documents;
using kanalitik.utility;
using MongoDB.Bson;
using MongoDB.Driver;
using System.Collections.Generic;

namespace kanalitik.model.test
{

    [TestClass]
    public class ConfigMailAccountModelTest
    {
        ConfigMailAccountModel model = new ConfigMailAccountModel();

        [TestMethod]
        public void createOneTest()
        {
            var data = new ConfigMailAccount
            {
                sender_name = "DONOTREPLY",
                email = "cr30active@gmail.com",
                password = "useRc30active",
                created_by = model.IdGuid, // id user login
                modified_by = model.IdGuid, // id user login
                created_date = DateTime.UtcNow,
                modified_date = DateTime.UtcNow
            };
            var actual = model.createOne(data).GetAwaiter().GetResult();
            var expectedResultName = "DONOTREPLY";
            Assert.AreEqual(expectedResultName, actual.sender_name);
        }

        [TestMethod]
        public void updateOneTest()
        {

            var actual = model.updateOne(new ConfigMailAccount
            {
                id = new ObjectId("554a01039c876e1fbc32a3ce"),
                sender_name = "ADMIN",
                email = "cr30active@gmail.com",
                password = "useRc30active",
                created_by = model.IdGuid, // id user login
                modified_by = model.IdGuid, // id user login
                created_date = DateTime.UtcNow,
                modified_date = DateTime.UtcNow
            }).GetAwaiter().GetResult();

            Assert.AreEqual("ADMIN", actual.sender_name);
        }

        [TestMethod]
        public void createMany()
        {
            // TODO
        }

        [TestMethod]
        public void updateManyTest()
        {
            // TODO      
        }

        [TestMethod]
        public void getByIdTest()
        {
            //var actual = model.getById("55471eab9c876e1720763ee8").GetAwaiter().GetResult();
            //Assert.AreEqual("55471eab9c876e1720763ee8", actual.id.ToString());
        }

        [TestMethod]
        public void deleteByIdTest()
        {
            //var actual = model.deleteById("55471eab9c876e1720763ee8").GetAwaiter().GetResult();
            //Assert.AreEqual("55471eab9c876e1720763ee8", actual);
        }

    }
}
