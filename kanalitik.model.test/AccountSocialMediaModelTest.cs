﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using kanalitik.models;
using kanalitik.data.access.documents;
using kanalitik.utility;
using MongoDB.Bson;
using MongoDB.Driver;
using System.Collections.Generic;

namespace kanalitik.model.test
{

    [TestClass]
    public class AccountSocialMediaModelTest
    {
        AccountSocialMediaModel model = new AccountSocialMediaModel();

        [TestMethod]
        public void createOneTest()
        {
            var data = new AccountSocialMedia
            {
                account_name = "Creoactive",
                client_id = new ObjectId().ToString(),
                email = "cr30active@gmail.com",
                expired_date = DateTime.UtcNow.AddDays(5),
                expired_in = 3000,
                fb_page_category = "",
                fb_page_permission = "",
                is_active = true,
                is_have_youtube_account = true,
                is_non_authorize = true,
                media = "Facebook",
                parent_social_media_id = "112233456",
                profile_picture = "",
                refresh_token = "",
                social_media_id = "",
                token_access = "",
                token_secret = "",


                created_by = model.IdGuid, // id user login
                modified_by = model.IdGuid, // id user login
                created_date = DateTime.UtcNow,
                modified_date = DateTime.UtcNow,
            };

            var actual = model.createOne(data).GetAwaiter().GetResult();
            //Assert.AreEqual("Creoactive", actual.name);
        }

        [TestMethod]
        public void updateOneTest()
        {

            var actual = model.updateOne(new AccountSocialMedia
            {
                id = new ObjectId("5549f65ad2b8de5904138d89"),
                account_name = "Creoactive",
                client_id = new ObjectId().ToString(),
                email = "cr30active@gmail.com",
                expired_date = DateTime.UtcNow.AddDays(5),
                expired_in = 3000,
                fb_page_category = "",
                fb_page_permission = "",
                is_active = true,
                is_have_youtube_account = true,
                is_non_authorize = true,
                media = "Twitter",
                parent_social_media_id = "546456789789",
                profile_picture = "",
                refresh_token = "",
                social_media_id = "",
                token_access = "",
                token_secret = "",



                modified_by = model.IdGuid, // id user login
                modified_date = DateTime.UtcNow,
            }).GetAwaiter().GetResult();

            //Assert.AreEqual("Creoactive Update", actual.name);
        }

        [TestMethod]
        public void createMany()
        {
            // TODO
        }

        [TestMethod]
        public void updateManyTest()
        {
            // TODO      
        }

        [TestMethod]
        public void getByIdTest()
        {
            var actual = model.getById("55408db70234c92a9477c008").GetAwaiter().GetResult();
            Assert.AreEqual("55408db70234c92a9477c008", actual.id.ToString());
        }

        //[TestMethod]
        //public void isEmailExistTest()
        //{
        //    var actual = model.isExist("mail@mail.com").GetAwaiter().GetResult();
        //    Assert.AreEqual("mail@mail.com", actual.email);
        //}

        [TestMethod]
        public void deleteByIdTest()
        {
            var actual = model.deleteById("55408db70234c92a9477c008").GetAwaiter().GetResult();
            Assert.AreEqual("55408db70234c92a9477c008", actual);
        }

        [TestMethod]
        public void listTest()
        {
            var actual = model.list(new BsonDocument(new BsonElement("email", "tru3.d3v@gmail.com")), new BsonDocument(new BsonElement("name", -1)), 0, 1).GetAwaiter().GetResult();

            Assert.AreEqual(1, actual.Length);

        }

    }
}
