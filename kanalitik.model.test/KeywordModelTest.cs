﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using kanalitik.models;
using kanalitik.data.access.documents;
using kanalitik.utility;
using MongoDB.Bson;
using MongoDB.Driver;
using System.Collections.Generic;

namespace kanalitik.model.test
{

    [TestClass]
    public class KeywordModelTest
    {
        KeywordModel model = new KeywordModel();

        [TestMethod]
        public void createOneTest()
        {
            var data = new Keyword
            {
                keyword = "doragorador",
                created_by = model.IdGuid, // id user login
                modified_by = model.IdGuid, // id user login
                created_date = DateTime.UtcNow,
                modified_date = DateTime.UtcNow
            };
            var actual = model.createOne(data).GetAwaiter().GetResult();
            var expectedResultName = "doragorador";
            Assert.AreEqual(expectedResultName, actual.keyword);
        }

        [TestMethod]
        public void updateOneTest()
        {

            var actual = model.updateOne(new Keyword
            {
                id = new ObjectId("554ade899c876e1bc05f485c"),
                keyword = "doragoradors",
                created_by = model.IdGuid, // id user login
                modified_by = model.IdGuid, // id user login
                created_date = DateTime.UtcNow,
                modified_date = DateTime.UtcNow
            }).GetAwaiter().GetResult();

            Assert.AreEqual("doragoradors", actual.keyword);
        }

        [TestMethod]
        public void createMany()
        {
            // TODO
        }

        [TestMethod]
        public void updateManyTest()
        {
            // TODO      
        }

        [TestMethod]
        public void getByIdTest()
        {
            var actual = model.getById("55471eab9c876e1720763ee8").GetAwaiter().GetResult();
            Assert.AreEqual("55471eab9c876e1720763ee8", actual.id.ToString());
        }

        [TestMethod]
        public void deleteByIdTest()
        {
            var actual = model.deleteById("55471eab9c876e1720763ee8").GetAwaiter().GetResult();
            Assert.AreEqual("55471eab9c876e1720763ee8", actual);
        }

    }
}
