﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using kanalitik.models;
using kanalitik.data.access.documents;
using kanalitik.utility;
using MongoDB.Bson;
using MongoDB.Driver;
using System.Collections.Generic;

namespace kanalitik.model.test
{

    [TestClass]
    public class LogEmailModelTest
    {
        LogEmailModel model = new LogEmailModel();

        [TestMethod]
        public void createOneTest()
        {
            var data = new LogEmail
            {
                sender = "ADMIN",
                to = "puputangelina@gmail.com",
                subject = "Kanalitik - User Activation",
                body = "Isi Body",
                created_by = model.IdGuid, // id user login
                modified_by = model.IdGuid, // id user login
                created_date = DateTime.UtcNow,
                modified_date = DateTime.UtcNow
            };
            var actual = model.createOne(data).GetAwaiter().GetResult();
            var expectedResultName = "ADMIN";
            Assert.AreEqual(expectedResultName, actual.sender);
        }

        [TestMethod]
        public void updateOneTest()
        {

            var actual = model.updateOne(new LogEmail
            {
                id = new ObjectId("5549eb9b9c876e0fbce6fce6"),
                sender = "ADMIN",
                to = "fahmi@gmail.com",
                subject = "Kanalitik - User Activation",
                body = "Isi Body",
                created_by = model.IdGuid, // id user login
                modified_by = model.IdGuid, // id user login
                created_date = DateTime.UtcNow,
                modified_date = DateTime.UtcNow
            }).GetAwaiter().GetResult();

            Assert.AreEqual("fahmi@gmail.com", actual.to);
        }

        [TestMethod]
        public void createMany()
        {
            // TODO
        }

        [TestMethod]
        public void updateManyTest()
        {
            // TODO      
        }

        [TestMethod]
        public void getByIdTest()
        {
            //var actual = model.getById("55471eab9c876e1720763ee8").GetAwaiter().GetResult();
            //Assert.AreEqual("55471eab9c876e1720763ee8", actual.id.ToString());
        }

        [TestMethod]
        public void deleteByIdTest()
        {
            //var actual = model.deleteById("55471eab9c876e1720763ee8").GetAwaiter().GetResult();
            //Assert.AreEqual("55471eab9c876e1720763ee8", actual);
        }

    }
}
