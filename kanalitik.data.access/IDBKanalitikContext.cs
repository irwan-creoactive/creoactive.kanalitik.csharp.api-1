﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using kanalitik.data.access.documents;
using MongoDB.Driver;

namespace kanalitik.data.access
{
    public interface IDBKanalitikContext
    {
        IMongoClient Client { get; }
        IMongoCollection<User> Users { get; }
        IMongoCollection<AccountSocialMedia> AccountSocialMedia { get; }
        IMongoCollection<Blog> Blog { get; }
        IMongoCollection<BlogSubscribe> BlogSubscribe { get; }
        IMongoCollection<Cms> Cms { get; }
        IMongoCollection<CodeGenerate> CodeGenerate { get; }
        IMongoCollection<ConfigMailAccount> ConfigMailAccount { get; }
        IMongoCollection<ConfigMessageContent> ConfigMessageContent { get; }
        IMongoCollection<DiscussionBoard> DiscussionBoard { get; }
        IMongoCollection<FeedbackClient> FeedbackClient { get; }
        IMongoCollection<GeneralConfiguration> GeneralConfiguration { get; }
        IMongoCollection<Inquiry> Inquiry { get; }
        IMongoCollection<InternetBankMessage> InternetBankMessage { get; }
        IMongoCollection<Keyword> Keyword { get; }
        IMongoCollection<LogEmail> LogEmail { get; }
        IMongoCollection<Module> Modules { get; }
        IMongoCollection<Feature> Feature { get; }
        IMongoCollection<Package> Package { get; }
        IMongoCollection<PackageOrder> PackageOrder { get; }
        IMongoCollection<PastSchedule> PastSchedule { get; }
        IMongoCollection<PostSchedule> PostSchedule { get; }
        IMongoCollection<RequestDemo> RequestDemo { get; }
        IMongoCollection<RoleUser> RoleUser { get; }
        IMongoCollection<SmsInbox> SmsInbox { get; }
        IMongoCollection<Stream> Stream { get; }
        IMongoCollection<StreamMenu> StreamMenu { get; }
        IMongoCollection<StreamUnwanted> StreamUnwanted { get; }
        IMongoCollection<UserLogActivity> UserLogActivity { get; }
        IMongoCollection<LogPop3Email> LogPop3Email { get; }
    }
}
