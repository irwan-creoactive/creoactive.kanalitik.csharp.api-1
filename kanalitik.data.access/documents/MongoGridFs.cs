﻿using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.GridFS;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kanalitik.data.access.documents
{
    public class MongoGridFs
    {
        private readonly MongoDatabase _db;
        private readonly MongoGridFS _gridFs;

        public MongoGridFs()
        {
            var conStr = ConfigurationManager.AppSettings["MongoDBHost"];
            var conDb = ConfigurationManager.AppSettings["MongoDBName"];
            var server = MongoServer.GetAllServers().First();
            _db = server.GetDatabase(conDb);
            _gridFs = _db.GridFS;
        }

        public ObjectId uploadFile(System.IO.Stream fileStream, string fileName)
        {
            var fileInfo = _gridFs.Upload(fileStream, fileName);
            return (ObjectId)fileInfo.Id;
        }

        public System.IO.Stream getFile(ObjectId id)
        {
            var file = _gridFs.FindOneById(id);
            return file.OpenRead();
        }
    }
}
