﻿using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace kanalitik.data.access.documents
{
    public class FeedbackClient : Base
    {
        /**
        * description : nama lainnya adalah ticketing; istilah untuk menampung segala bentuk keluhan dan pertanyaan yang diajukan oleh user yang sudah memiliki package. bisa juga dibilang collection ini menampung segala bentuk customer service yang terjadi antara pengguna dengan pengelola kanalitik.
        */
        /// <summary>
        /// object_id collection feedback_client
        /// </summary>
        public ObjectId id { set; get; } 
        /// <summary>
        /// user id, diambil dari collection user.obejct_id
        /// </summary>
        public string customer_login { set; get; } 
        /// <summary>
        /// username yang mengisi pertanyaan 
        /// </summary>
        public string username { set; get; } 
        /// <summary>
        /// category feedback
        /// </summary>
        public string category { set; get; } 
        /// <summary>
        /// isian pesan atau keluhan atau pertanyaan 
        /// </summary>
        public string message { set; get; } 
        /// <summary>
        /// file attachment
        /// </summary>
        public string attachment { set; get; } 
        /// <summary>
        /// berisi open - close; open - jika feedback ini masih belum memuaskan user client; close - jika feedback ini tidak mendapat jawaban selama 2 minggu atau user client terpuaskan;
        /// </summary>
        public string status { set; get; } 
        /// <summary>
        /// user yang melakukan perubahan status; berisi user id yang diambil dari object-id collection user;
        /// </summary>
        public string modify_status { set; get; } 
        /// <summary>
        /// berisi array object feedback; merupakan balasan untuk setiap feedback yang dilakukan oleh user client; feedback ini berisi balasan dari admin kanalitik dan user yang membuat feedback ini; 
        /// </summary>
        public List<Feedback> feedbacks { set; get; } 
    }

    public class Feedback
    {
        /// <summary>
        /// user id yang melakukan balasan terhadap ticket; diambil dari object-id collection user
        /// </summary>
        public string user_id { set; get; } 
        /// <summary>
        /// username yang melakukan  balasan terhadap ticket;
        /// </summary>
        public string username { set; get; } 
        /// <summary>
        /// isian comment balasan 
        /// </summary>
        public string comment { set; get; } 
        /// <summary>
        /// attachment yang menyertai balasan
        /// </summary>
        public string attachment { set; get; } 
        /// <summary>
        /// id user pembuat data
        /// </summary>
        public string created_by { set; get; } 
        /// <summary>
        /// id user yang terakhir merubah data
        /// </summary>
        public string modified_by { set; get; } 
        /// <summary>
        /// tanggal pembuatan data
        /// </summary>
        public string created_date { set; get; } 
        /// <summary>
        /// tanggal perubahan data
        /// </summary>
        public string modified_date { set; get; } 
    }
}
