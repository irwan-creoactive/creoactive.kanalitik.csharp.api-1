﻿using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace kanalitik.data.access.documents
{
    public enum OrderStatusEnum { Close = 1, Open = 0 }
    public enum PackageStatusEnum { Paid = 1, Unpaid = 0 }


    /// <summary>
    /// merupakan collection yang menampung data order package			
    /// </summary>
    public class PackageOrder : Base
    {
        /// <summary>
        /// object id collection package_order
        /// </summary>
        [BsonId]
        public ObjectId id { set; get; }

        /// <summary>
        /// package yang dipesan diambil dari collection package._id
        /// </summary>
        public string package_id { set; get; }

        /// <summary>
        /// user yang memesan package; diambil dari collection user
        /// </summary>
        public string user_id { set; get; }

        /// <summary>
        /// client yang akan disematkan package; diambil dari collection client
        /// </summary>
        public string client_id { set; get; }

        /// <summary>
        /// status pemesanan package; berisi close - open; close jika pemesanan dianggap selesai; open jika pemesanan masih belum selesai
        /// </summary>
        public string order_status { set; get; }

        /// <summary>
        /// type order berisi order - renew - upgrade - downgrade;
        /// </summary>
        public string order_type { set; get; }

        /// <summary>
        /// ini adalah isian dari user, jika pilihannya upgrade/downgrade, atribut ini terisi;
        /// </summary>
        public string order_type_start { set; get; }

        /// <summary>
        /// ini adalah isian dari sistem. Jika ternyata yang dipilih adalah downgrade dan tidak memiliki outstanding payment;
        /// </summary>
        public string order_type_end { set; get; }

        /// <summary>
        /// status pembayaran package; berisi paid - unpaid; paid jika package order sudah dibayar; unpaid jika package order belum dibayar;
        /// </summary>
        public string status { set; get; }

        /// <summary>
        /// berisi true - false; true jika package_order ini sudah dieksekusi; false jika package order ini belum dieksekusi;
        /// </summary>
        public string activate_status { set; get; }

        /// <summary>
        /// kode invoice (nomor urut)
        /// </summary>
        public string invoice { set; get; }

        /// <summary>
        /// tanggal package order tidak berlaku. jika sudah lewat tanggal ini, order_status akan berisi close dengan status berisi unpaid.
        /// </summary>
        public DateTime expired_at { set; get; }
    }

}
