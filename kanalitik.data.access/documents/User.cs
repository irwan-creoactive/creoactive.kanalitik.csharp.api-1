﻿using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;


namespace kanalitik.data.access.documents
{
    public class User:Base
    {
   
       
        public ObjectId id { set; get; }

        public string name { set; get; }

        public string email { set; get; }

        public string password { set; get; }

        public string profile_picture { set; get; }

        public string business_type { set; get; }

        public string business_type_name { set; get; }

        public string contact_number { set; get; }

        public string[] client { set; get; }

        public string role_user { set; get; }

        
    }
}
