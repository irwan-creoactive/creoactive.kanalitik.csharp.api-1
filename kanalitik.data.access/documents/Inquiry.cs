﻿using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace kanalitik.data.access.documents
{
    public class Inquiry : Base
    {
        /**
        * description : collection ini berisi daftar pesan yang dikirimkan oleh pengunjung melalui web public. Pengunjung adalah orang-orang yang membuka website kanalitik tanpa melakukan registrasi atau pun login.
        */

        /// <summary>
        /// object id collection inquiry
        /// </summary>
        public ObjectId id { set; get; }
        /// <summary>
        /// alamat email pengunjung yang melakukan inquiry
        /// </summary>
        public string email { set; get; }
        /// <summary>
        /// nama pengunjung yang melakukan inquiry
        /// </summary>
        public string name { set; get; } 
        /// <summary>
        /// nomor telepon pengunjung yang melakukan inquiry
        /// </summary>
        public string phone_number { set; get; }
        /// <summary>
        /// isi pesan yang dimasukkan 
        /// </summary>
        public string message { set; get; } 

    }
}
