﻿using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace kanalitik.data.access.documents
{
    public class LogPop3Email : Base
    {
        /**
        * description : inbox email dari email yang didaftarkan;
        */
        public ObjectId id { set; get; }
        /// <summary>
        /// object id collection log_pop3_email
        /// </summary>

        public string from { set; get; }
        /// <summary>
        /// alamat pengirim email
        /// </summary>

        public string subject { set; get; }
        /// <summary>
        /// judul email
        /// </summary>

        public string body { set; get; }
        /// <summary>
        /// isi email
        /// </summary>

    }
}
