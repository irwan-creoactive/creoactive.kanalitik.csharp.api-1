﻿using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace kanalitik.data.access.documents
{
    public class BlogSubscribe : Base
    {
        /**
        * description : collection ini berisi daftar email yang melakukan subscribe pada blog; Setiap postingan anyar akan langsung diinfokan pada email yang terdaftar pada collection ini.
        */

        public ObjectId id { set; get; }
        /// <summary>
        /// object id collection blog_subscribe
        /// </summary>

        public string email { set; get; }
        /// <summary>
        /// nama/alamat email 
        /// </summary>

    }
}
