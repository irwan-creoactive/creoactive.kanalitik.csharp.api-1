﻿using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace kanalitik.data.access.documents
{
    public class Cms : Base
    {
        /**
        * description : collection ini menampung data yang akan digunakan pada web public. seluruh tulisan dan penjelasan yang muncul pada web public, ditampung di sini. Saat ini digunakan untuk 2 penggunaan. Laman pesan dan laman web public.
        */
        /// <summary>
        /// object id dari collection cms
        /// </summary>
        public ObjectId id { set; get; }
        /// <summary>
        /// saat ini penggunaannya terbagi menjadi 2 category, content_static - untuk penggunaan pesan; dan cms untuk pengelolaan content web public
        /// </summary>
        public string category { set; get; }
        /// <summary>
        /// code unik cms yang digunakan sebagai pembacaan sistem untuk ditampilkan.
        /// </summary>
        public string component { set; get; }      
        /// <summary>
        /// content yang akan ditampilkan dalam bahasa inggris
        /// </summary>
        public string content { set; get; } 
        /// <summary>
        // tanggal package mulai aktif 
        /// </summary>
        public string content_in { set; get; } 
    }
}
