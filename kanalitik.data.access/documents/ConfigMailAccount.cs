﻿using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace kanalitik.data.access.documents
{
    public class ConfigMailAccount : Base
    {
        /**
        * description : berisi konfigurasi email yang digunakan untuk mengirim email yang terdapat pada collection log_email;
        */

        public ObjectId id { set; get; }
        /// <summary>
        /// object_id collection config_mail_account
        /// </summary>

        public string sender_name { set; get; }
        /// <summary>
        /// nama alias
        /// </summary>


        public string email { set; get; }
        /// <summary>
        /// alamat email
        /// </summary>


        public string password { set; get; }
        /// <summary>
        /// password
        /// </summary>
    }
}
