﻿using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace kanalitik.data.access.documents
{
    /// <summary>
    /// collection ini menyimpan seluruh code generate terkait flow sistem; misal untuk aktivasi akun, user akan dikirimi kode khusus untuk aktivasi, atau ketika user meminta untuk reset password user juga akan menerima kode unik reset password; kode-kode ini lah yang akan disimpan pada collection code_generate			
    /// </summary>
    public class CodeGenerate : Base
    {
        /// <summary>
        /// object id dari collection code_generate
        /// </summary>
        public ObjectId id { set; get; }
        /// <summary>
        /// user yang dikirimi code_generate
        /// </summary>
        public string user { set; get; }
        /// <summary>
        /// jenis code generate; bisa berisi "activation_user" atau "reset_password" sesuai dengan peruntukkan prosesnya;
        /// </summary>
        public string type { set; get; }
        /// <summary>
        /// isian kode uniknya;
        /// </summary>
        public string code { set; get; }
        /// <summary>
        /// batas akhir penggunaan code_generate; setelah melewati waktu ini, kode tersebut akan expire dan tidak bisa digunakan kembali;
        /// </summary>
        public DateTime due_date { set; get; }
    }
}
