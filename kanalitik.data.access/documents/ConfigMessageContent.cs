﻿using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace kanalitik.data.access.documents
{
    public class ConfigMessageContent : Base
    {
        /**
        * description : berisi konfigurasi untuk pesan yang dikirim lewat email yang pesan yang tampil pada web public; penggabungan dari collection config_mail_content dan config_message;
        */

        public ObjectId id { set; get; }
        /// <summary>
        /// object id collection config_mesage_content
        /// </summary>

        public string message_type { set; get; }
        /// <summary>
        /// berisi mail - message; 
        /// mail jika konfigurasi digunakan untuk mengirim email; 
        /// message jika konfigurasi digunakan untuk menampilkan pesan 
        /// </summary>

        public string code { set; get; }
        /// <summary>
        /// kode unik config_message_content
        /// </summary>

        public string subject { set; get; }
        /// <summary>
        /// berisi judul email; 
        /// atribut ini diisi jika message_type berisi mail;
        /// </summary>

        public string message { set; get; }
        /// <summary>
        /// merupakan body email jika message_type mail; 
        /// dan merupakan message yang tampil jika message_type berisi message;
        /// </summary>

    }
}
