﻿using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace kanalitik.data.access.documents
{
    public class Blog : Base
    {
        /**
        * description : collection ini menampung isian blog yang akan tampil pada web public;
        */

        /// <summary>
        /// object id collection blog
        /// </summary>
        public ObjectId id { set; get; }

        /// <summary>
        /// judul blog
        /// </summary>
        public string title { set; get; }

        /// <summary>
        /// isi blog
        /// </summary>
        public string content { set; get; }

        /// <summary>
        /// gambar yang disertakan pada blog; 
        /// gambar ini akan tampil pda laman depan blog
        /// </summary>
        public string image { set; get; }

        /// <summary>
        /// berisi array object comment; 
        /// merupakan daftar komen yang melekat pada setiap postingan blog
        /// </summary>
        public List<BlogComment> comments { set; get; }

        /// <summary>
        /// berisi true - false; 
        /// true - jika post sudah publish, efeknya blog post ini akan tampil; 
        /// false - jika blog post belum ditampilkan
        /// </summary>
        bool is_publish { set; get; }

    }

    public class BlogComment
    {
        /// <summary>
        /// aurutan comment
        /// </summary>
        string seq { set; get; }

        /// <summary>
        /// alamat email yang memberikan komentar
        /// </summary>
        string email { set; get; }

        /// <summary>
        /// nama yang memberikan komentar
        /// </summary>
        string name { set; get; }

        /// <summary>
        /// isian komentar
        /// </summary>
        string comment { set; get; }

        /// <summary>
        /// berisi true - false; 
        /// true - jika komentar ini sudah diapprove, efeknya comment ini akan tampil; 
        /// false - jika komentar ini tidak/belum diapprove;
        /// </summary>
        bool is_approve { set; get; }

        /// <summary>
        /// id user pembuat data
        /// </summary>
        public string created_by { set; get; }

        /// <summary>
        /// id user yang terakhir merubah data
        /// </summary>
        public string modified_by { set; get; }

        /// <summary>
        /// tanggal pembuatan data
        /// </summary>
        public string created_date { set; get; }

        /// <summary>
        /// tanggal perubahan data
        /// </summary>
        public string modified_date { set; get; }

    }
}
