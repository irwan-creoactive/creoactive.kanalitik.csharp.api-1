﻿using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace kanalitik.data.access.documents
{
    public class Keyword : Base
    {
        /**
        * description : collection ini menampung daftar keyword yang dimasukkan oleh user;			
        */

        public ObjectId id { set; get; } 
        /// <summary>
        /// obejct id dari collection keyword
        /// </summary>

        public string client_id { set; get; } 
        /// <summary>
        /// berisi object id collection client
        /// </summary>

        public string keyword { set; get; } 
        /// <summary>
        /// nama keyword
        /// </summary>

        public List<SocialMedia> social_medias { set; get; } 
        /// <summary>
        /// berisi array daftar social media dimana keyword ini dicari; 
        /// saat ini keyword dapat dicari pada twitter, google+, youtube dan instagram;
        /// </summary>
        
    }

    public class SocialMedia
    {
        string social_media { set; get; } 
        /// <summary>
        /// diisi twitter, google+, youtube atau instagram;
        /// </summary>
        
    }
}
