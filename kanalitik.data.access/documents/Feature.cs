﻿using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace kanalitik.data.access.documents
{
    public class Feature : Base
    {
        /**
        * description : collection ini menampung daftar menu yang ada pada aplikasi kanalitik. Menu yang disimpan tidak hanya menu sistem, melainkan menu stream yang dibuat oleh user juga dimasukkan pada collection ini.			
        */

        public ObjectId id { set; get; }
        /// <summary>
        /// object id dari collection feature
        /// </summary>

        public string code { set; get; }
        /// <summary>
        /// kode feature 
        /// </summary>

        public string name { set; get; }
        /// <summary>
        /// nama feature
        /// </summary>

        public string seq { set; get; }
        /// <summary>
        /// urutan kemunculan feature
        /// </summary>

        public string description { set; get; }
        /// <summary>
        /// penjelasan singkat tentang feature
        /// </summary>

        public string parent_feature_code { set; get; }
        /// <summary>
        /// berisi object code dari collection feature; 
        /// atribut ini diisi jika feature ini merupakan anak dari feature yang lain.
        /// </summary>

    }
}
