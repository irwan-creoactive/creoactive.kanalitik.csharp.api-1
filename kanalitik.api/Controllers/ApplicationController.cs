﻿using Akka.Actor;
using kanalitik.akka.actors.core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace kanalitik.api.Controllers
{
    public abstract class ApplicationController : ApiController
    {
        public static readonly ActorSystem System = BootstrapActor.System;
        public static readonly TimeSpan DefaultTimeout = TimeSpan.FromSeconds(15);

        private readonly ICanTell _addressBook;

        protected ApplicationController()
        {
            _addressBook = System.GetAddressBook();
        }

        protected async Task<ICanTell> GetActor<T>() where T : ActorBase
        {
            var result = await _addressBook.Ask<AddressBook.Found>(new AddressBook.Get(typeof(T)));
            return result.Ref;
        }
    }
}
